import { config } from '../config/index';
import { sendCustomEmail } from './custom_email.util';
import {
	getCollectionDocData,
	updateCollectionDoc,
} from './generic-collection-data.util';
import { collectionNames } from './externalized_variables.util';
import {
	logOnFirestoreDocumentGetError,
	logOnFirestoreDocumentUpdateError,
} from './logger.util';
import { quoteInvitationEmailTemplate } from '../email-templates/invitation_to_quote';

export function sendEmailToInvitedEmails(quoteData: FirebaseFirestore.DocumentData) {
	const { uid, toInviteEmails, invitedEmails, user } = quoteData;
	const quoteUserDataResponse = getCollectionDocData({
		colName: collectionNames.users,
		colDocUID: user,
	});
	quoteUserDataResponse.then(async quoteUserData => {
		const { firstName, lastName } = quoteUserData!;
		const toUpdateInvitedEmails = invitedEmails ? invitedEmails : [];
		let updatedToInviteEmails = toInviteEmails ? toInviteEmails : [];
		for await (const item of toInviteEmails) {
			const { email, invitedAt } = item;
			const emailContent = `You have been invited by 
                ${firstName} ${lastName} to check out their quote request! The quote description is as:`
			const mailData = {
				to: email,
				from: config.envvar.buzzarr_buzzy_email,
				subject: 'Someone invited you to see their quote request on Buzzarr!',
				text: '.',
				html: quoteInvitationEmailTemplate({ firstName, emailContent, quoteData }),
			}
			try {
				await sendCustomEmail(mailData);
				toUpdateInvitedEmails.push({ email: email, invitedAt: invitedAt });
				updatedToInviteEmails = updatedToInviteEmails.filter(
					(obj: { email: string, invitedAt: Date }) => obj.email !== email);
			} catch {
				toUpdateInvitedEmails.push({ email: email, invitedAt: invitedAt });
			};
		};
		try {
			const colDocData = { invitedEmails: toUpdateInvitedEmails, toInviteEmails: updatedToInviteEmails };
			await updateCollectionDoc({
				colName: collectionNames.quotes,
				colDocUID: uid,
				colDocData: colDocData,
			})
			console.log('Updated quote toInviteEmails and inviteeEmails are:::', colDocData);
		} catch (error) {
			logOnFirestoreDocumentUpdateError({
				functionName: 'sendEmailToInvitedEmails',
				message: "Error while updating invitedEmails of quote.",
				error,
			}, {
				operation: `${collectionNames.quotes}-update-error`, uid: user,
			})
		}
	}).catch(error => {
		logOnFirestoreDocumentGetError({
			functionName: 'sendEmailToInvitedEmails',
			message: "Error while getting user data of quote.",
			error,
		}, {
			operation: `${collectionNames.users}-query-error`, uid: user,
		})
	});
}
