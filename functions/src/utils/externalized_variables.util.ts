export const userAccountStatus = {
	active: 'active',
	hold: 'hold',
	flagged: 'flagged',
	disabled: 'disabled',
	deleted: 'deleted',
}

export const collectionNames = {
	users: 'users',
	quotes: 'quotes',
	conversations: 'conversations',
	proposals: 'proposals',
	locations: 'locations',
	categories: 'categories',
	admins: 'admins',
}

export const subCollectionNames = {
	userDetails: 'userDetails',
	userProfiles: 'userProfiles',
	providerDetails: 'providerDetails',
}
export const adminRoleNames = [
	'buzzarrAdmin',
]

export const userRoleNames = {
	buzzarrAdmin: 'buzzarrAdmin',
	providerUser: 'providerUser',
	registeredUser: 'registeredUser',
}

export const genericVariableNames = {
	registrationIsCompleted: 'registrationIsCompleted',
}

export const quoteStatus = {
	active: 'active',
	closed: 'closed',
	archived: 'archived',
}

export const proposalStatus = {
	active: 'active',
	accepted: 'accepted',
	declined: 'declined',
	withdrawn: 'withdrawn',
	archived: 'archived',
}

export const buzzarrEmailContentVariables = {
	companyName: "Buzzarr",
	zipCode: "93803",
	address: "Sports Arena",
	city: "San Diego",
	state: "California",
	country: "US",
}

export const locationStatus = {
	pending_approval: 'pending_approval',
	approved: 'approved',
}

export const environmentNames = {
	local: 'local',
	dev: 'dev',
	staging: 'staging',
	production: 'production',
}
