import sgMail = require('@sendgrid/mail');
import { config, logger } from '../config/index';
import { logSimpleMessage } from './logger.util';
const sendGridAPIKey = config.envvar.sendgrid_api_key || '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';
sgMail.setApiKey(sendGridAPIKey)

export interface EmailDataFormat {
	to: string,
	from: string,
	subject: string,
	text: string,
	html: string,
}

export async function sendCustomEmail(emailData: EmailDataFormat) {
	try {
		await sgMail
			.send(emailData);
		logger.info(`Email sent to ${emailData.to} with content ${emailData.html}`);
	} catch (error) {
		logger.error(`Error in sending Email sent to
             ${emailData.to} with content ${emailData.html}:::${JSON.stringify(error)}`, error, { operation: 'email-error' });
	}
}

export function isValidBuzzarrEmailDomain(email: string): boolean {
	let result = false;
	const emailParts = email.split('@');
	if (emailParts.length > 1) {
		const emailDomain = emailParts[1];
		if (emailDomain.includes('buzzarr.com')) {
			result = true;
		}
	}
	logSimpleMessage(`isValidBuzzarrEmailDomain:: ${result}`);
	return result;
}
