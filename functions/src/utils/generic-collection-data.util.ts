
import { firestoreDatabase } from '../config/index';
import {
    getGenericCollectionRef,
    getGenericCollectionDocumentRef,
} from './collection_functions.util'

export async function getCollectionDocData(parameters:
    { colName: string, colDocUID: string }): Promise<FirebaseFirestore.DocumentData | undefined> {
    const docRef = getGenericCollectionDocumentRef(parameters);
    const docSnap = await docRef.get();
    return docSnap.data()
};

export async function addNewCollectionDoc(parameters:
    { colName: string, colDocData: any }): Promise<FirebaseFirestore.DocumentReference<FirebaseFirestore.DocumentData>> {
    const { colName, colDocData } = parameters;
    return await firestoreDatabase.collection(colName)
        .add(colDocData);
}

export async function setCollectionDoc(parameters:
    { colName: string, colDocUID: string, colDocData: any }): Promise<FirebaseFirestore.WriteResult> {
    const { colName, colDocUID, colDocData } = parameters;
    const docRef = getGenericCollectionDocumentRef({ colName, colDocUID });
    return await docRef.set(colDocData,
        { merge: true });
}

export async function updateCollectionDoc(parameters:
    { colName: string, colDocUID: string, colDocData: any }): Promise<FirebaseFirestore.WriteResult> {
    const { colName, colDocUID, colDocData } = parameters;
    const docRef = getGenericCollectionDocumentRef({ colName, colDocUID });
    return await docRef.update(colDocData);
}

/**
* Queries the  collection for documents where 'key' equals 'value'.{ key: any, operator: any, value: any }
*/
export async function queryGenericCollection(parameters:
    { colName: string, queryList: Array<{ key: any, operator: any, value: any }> }
): Promise<FirebaseFirestore.QuerySnapshot<FirebaseFirestore.DocumentData>> {
    const { colName, queryList } = parameters;
    const collectionRef = getGenericCollectionRef({ colName })
    let querySnapshotResult: any = [];
    if (queryList.length === 0) {
        querySnapshotResult = await collectionRef.get();
    } else if (queryList.length === 1) {
        const query = queryList[0];
        querySnapshotResult = await collectionRef.where(query.key, query.operator, query.value).get();
    } else if (queryList.length === 2) {
        const query1 = queryList[0];
        const query2 = queryList[1];
        querySnapshotResult = await collectionRef
            .where(query1.key, query1.operator, query1.value)
            .where(query2.key, query2.operator, query2.value)
            .get();
    }
    else if (queryList.length === 3) {
        const query1 = queryList[0];
        const query2 = queryList[1];
        const query3 = queryList[2];
        querySnapshotResult = await collectionRef
            .where(query1.key, query1.operator, query1.value)
            .where(query2.key, query2.operator, query2.value)
            .where(query3.key, query3.operator, query3.value)
            .get();
    } else if (queryList.length === 4) {
        const query1 = queryList[0];
        const query2 = queryList[1];
        const query3 = queryList[2];
        const query4 = queryList[3];
        querySnapshotResult = await collectionRef
            .where(query1.key, query1.operator, query1.value)
            .where(query2.key, query2.operator, query2.value)
            .where(query3.key, query3.operator, query3.value)
            .where(query4.key, query4.operator, query4.value)
            .get();
    }
    return querySnapshotResult;
};


// switch (queryListLength) {
    //     case 0: {
    //         return querySnapshotResult = await collectionRef.get();
    //     }
    //     case 1: {
    //         const query = queryList[0];
    //         querySnapshotResult = await collectionRef.where(query.key, query.operator, query.value).get();
    //         break;
    //     }
    //     case 2: {
    //         const query1 = queryList[0];
    //         const query2 = queryList[1];
    //         querySnapshotResult = await collectionRef
    //             .where(query1.key, query1.operator, query1.value)
    //             .where(query2.key, query2.operator, query2.value)
    //             .get();
    //         break;
    //     }
    //     case 3: {
    //         const query1 = queryList[0];
    //         const query2 = queryList[1];
    //         const query3 = queryList[2];
    //         querySnapshotResult = await collectionRef
    //             .where(query1.key, query1.operator, query1.value)
    //             .where(query2.key, query2.operator, query2.value)
    //             .where(query3.key, query3.operator, query3.value)
    //             .get();
    //         break;
    //     }
    //     case 0: {
    //         const query1 = queryList[0];
    //         const query2 = queryList[1];
    //         const query3 = queryList[2];
    //         const query4 = queryList[3];
    //         querySnapshotResult = await collectionRef
    //             .where(query1.key, query1.operator, query1.value)
    //             .where(query2.key, query2.operator, query2.value)
    //             .where(query3.key, query3.operator, query3.value)
    //             .where(query4.key, query4.operator, query4.value)
    //             .get();
    //         break;
    //     }
    // }