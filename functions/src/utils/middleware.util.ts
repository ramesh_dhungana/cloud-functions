import { Request, Response } from "express";
import { firebaseAdmin } from "../config";
export interface LocalsAuthUserDataModel {
    userUID: string,
    // TODO: keep updating roles as they get added or removed here
    roles: Array<'buzzarrAdmin' | 'providerUser' | 'registeredUser'>,
    email: string,
}

export function isPOSTRequest(request: Request) {
    return request.method === 'POST';
}

export function isGETRequest(request: Request) {
    return request.method === 'GET';
}

export function methodNotAllowedResponse(response: Response) {
    return response.status(405).send({ success: false, message: 'Method Not Allowed!' });
}

export function isAuthenticated(request: Request, response: Response, next: Function) {
    const { authorization } = request.headers
    if (!authorization) {
        console.log('Unauthorized: No Authorization Header');
        return response.status(401).send({ success: false, message: 'Unauthorized' });
    }
    if (!authorization.startsWith('Bearer')) {
        console.log('Unauthorized: No Bearer Token');
        return response.status(401).send({ success: false, message: 'Unauthorized' });
    }
    const split = authorization.split('Bearer ')
    if (split.length !== 2) {
        console.log('Unauthorized: Invalid Authorization Header Format');
        return response.status(401).send({ success: false, message: 'Unauthorized' });
    }
    const token = split[1];
    try {
        return firebaseAdmin.auth().verifyIdToken(token)
            .then((decodedToken) => {
                console.log(`Decoded Token  ${JSON.stringify(decodedToken)}`);
                const authUserData = {
                    userUID: decodedToken.uid,
                    roles: decodedToken.roles,
                    email: decodedToken.email,
                }
                response.locals = { ...response.locals, authUserData }
                return next();
            })
            .catch((error) => {
                console.error(`Erro on firebaseAuth.verifyIdToken(${token}):: ${error}`);
                return response.status(401).send({ success: false, message: 'Unauthorized' });
            });;

    } catch (err) {
        console.error(`${err.code} -  ${err.message}`)
        return response.status(401).send({ success: false, message: 'Unauthorized' });
    }
}

export function hasAuthorizedRoles(opts:
    { hasRoles: Array<'buzzarrAdmin' | 'providerUser' | 'registeredUser'> },
    request: Request, response: Response, next: Function
) {
    // TODO: keep updating hasRoles as they get added or removed here
    const authUserData: LocalsAuthUserDataModel = response.locals.authUserData;
    const { roles } = authUserData;

    //TODO: look into allowSameUser things later. id is param passed in the route eg. app.get('/test/:id', function(req,res){} );
    // const { uid } = request.params;

    // if authenticated user has no roles, user is not authorized
    if (!roles.length) {
        return response.status(401).send({ success: false, message: 'Unauthorized Role' })
    }

    // if authenticated user has any of the roles that are provided for the endpoint, user is authorized.
    if (roles.some(role => opts.hasRoles.indexOf(role) > -1)) {
        return next();
    }
    return response.status(401).send({ success: false, message: 'Unauthorized Role' })

}