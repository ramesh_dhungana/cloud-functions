
import {
    getGenericSubCollectionDocumentRef,
} from './collection_functions.util'

export async function getSubCollectionDocData(parameters:
    { colName: string, colDocUID: string, subColName: string, subColDocUID: string }): Promise<FirebaseFirestore.DocumentData | undefined> {
    const docRef = getGenericSubCollectionDocumentRef(parameters);
    const docSnap = await docRef.get();
    return docSnap.data()
};

