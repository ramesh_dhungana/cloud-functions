import { firebaseAdmin } from "../config";

export async function disableAuthUser(userUID: string) {
    await firebaseAdmin
        .auth()
        .updateUser(userUID, {
            disabled: true,
        });
}

export async function enableAuthUser(userUID: string) {
    await firebaseAdmin
        .auth()
        .updateUser(userUID, {
            disabled: false,
        });
}