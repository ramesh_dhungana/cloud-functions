import { config, firebaseAdmin } from "../config";
import { sendCustomEmail } from "./custom_email.util";

export function sendEmailToAdminOnNewAdminCreation(userUID: string, type: 'create' | 'update') {
	let subject: string;
	let emailContent: string;
	firebaseAdmin
		.auth()
		.getUser(userUID)
		.then(async (userRecord) => {
			// See the UserRecord reference doc for the contents of userRecord.
			console.log(`Successfully fetched user data: ${userRecord.toJSON()}`);
			switch (type) {
				case 'create':
					subject = 'Buzzarr | New Admin added to project!'
					emailContent = `
					Hi,  
					<p> New Admin has been added to the project. The information of new admin is as: </p>
					${JSON.stringify(userRecord)}
					`
				case 'update':
					subject = 'Buzzarr | Admin data of project updated!';
					emailContent = `
					Hi,  
					<p> Admin has been updated. The information of admin is as: </p>
					${JSON.stringify(userRecord)}`
			}
			const email = userRecord.email as string;
			let mailData = {
				to: email,
				from: config.envvar.buzzarr_noreply_email,
				subject: subject,
				text: '.',
				html: emailContent,
			}
			// to new admin
			await sendCustomEmail(mailData);
			// to super admin
			mailData = { ...mailData, to: config.envvar.buzzarr_admin_email }
			await sendCustomEmail(mailData);
		})
		.catch((error) => {
			console.log('Error fetching user data:', error);
		});

}