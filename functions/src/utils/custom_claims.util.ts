import { uniq } from "lodash";
import { firebaseAdmin } from "../config";
import { genericVariableNames } from "./externalized_variables.util";

export async function setCustomClaims(userUID: string, customClaims: any) {
	try {
		await firebaseAdmin.auth().setCustomUserClaims(userUID, customClaims);
		console.log(`Success on setCustomClaims(${userUID},${JSON.stringify(customClaims)})`);

	} catch (error) {
		console.log(`Error on getUserCurrentCustomClaims(${userUID}): ${JSON.stringify(error)}`);

	}
}

export async function getUserCurrentCustomClaims(userUID: string) {
	return await firebaseAdmin.auth()
		.getUser(userUID)
		.then((user) => {
			const currentCustomClaims = user.customClaims;
			console.log(`Success on getUserCurrentCustomClaims(${userUID}):with data ${JSON.stringify(user)} and custom claims ${JSON.stringify(currentCustomClaims)}`);
			return currentCustomClaims ? currentCustomClaims : {};
		})
		.catch((error) => {
			console.log(`Error on getUserCurrentCustomClaims(${userUID}): ${JSON.stringify(error)}`);
			return null;
		});
}

export async function setRegistrationStepsCustomClaims(userUID: string, claimData: { key: string, value: boolean }) {
	const userCurrentCustomClaims = await getUserCurrentCustomClaims(userUID);
	if (userCurrentCustomClaims) {
		const registrationSteps = userCurrentCustomClaims['registrationSteps'] ? userCurrentCustomClaims['registrationSteps'] : {};
		registrationSteps[claimData['key']] = claimData['value'];
		userCurrentCustomClaims['registrationSteps'] = registrationSteps;
		await setCustomClaims(userUID, userCurrentCustomClaims);
	} else {
		console.log(`Error on setRegistrationStepsCustomClaims(${userUID},${claimData}): userCurrentCustomClaims is null`);
	}
}

export async function setUserRolesCustomClaims(userUID: string, roleName: string) {
	const userCurrentCustomClaims = await getUserCurrentCustomClaims(userUID);
	if (userCurrentCustomClaims) {
		const currentRolesClaims = userCurrentCustomClaims['roles'] ? userCurrentCustomClaims['roles'] : [];
		currentRolesClaims.push(roleName);
		userCurrentCustomClaims['roles'] = uniq(currentRolesClaims);
		await setCustomClaims(userUID, userCurrentCustomClaims);
	} else {
		console.log(`Error on setUserRolesCustomClaims(${userUID},${roleName}): userCurrentCustomClaims is null`);
	}
}

export async function setRegistrationIsCompletedCustomClaims(userUID: string, registrationIsCompleted: boolean) {
	const userCurrentCustomClaims = await getUserCurrentCustomClaims(userUID);
	if (userCurrentCustomClaims) {
		userCurrentCustomClaims[genericVariableNames.registrationIsCompleted] = registrationIsCompleted;
		await setCustomClaims(userUID, userCurrentCustomClaims);
	} else {
		console.log(`Error on setRegistrationIsCompletedCustomClaims(${userUID},${registrationIsCompleted}): userCurrentCustomClaims is null`);
	}
}

export async function setAccountStatusDeletedCustomClaims(userUID: string) {
	const userCurrentCustomClaims = await getUserCurrentCustomClaims(userUID);
	if (userCurrentCustomClaims) {
		userCurrentCustomClaims['account_deleted'] = true;
		await setCustomClaims(userUID, userCurrentCustomClaims);
	} else {
		console.log(`Error on setAccountStatusDeletedCustomClaims(${userUID}): userCurrentCustomClaims is null`);
	}
}

export async function setAdminCustomClaims(uid: string, roleName: string) {
	return await setUserRolesCustomClaims(uid, roleName);
}