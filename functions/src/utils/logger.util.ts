import { logger } from '../config/index';

export const logOperationsType = {
	locationsEnabled: 'locations-enabled',
	locationsDisabled: 'locations-disabled',
	usersAccountStatusChanged: 'users-account-status-changed',
}

export function logSimpleMessage(message: string) {
	logger.info(message);
}

export function logOnCollectionDocumentCreate(parameters: { colName: string, colDocUID: string, colDocData: any },
	logJsonPayload: { operation: string, uid: string }) {
	const { colName, colDocUID, colDocData } = parameters;
	logger.info(`${colName}/${colDocUID} created with data: 
            ${JSON.stringify(colDocData)}`, logJsonPayload);
}

export function logOnSubCollectionDocumentCreate(parameters: {
	colName: string, colDocUID: string, subColName: string,
	subColDocUID: string, subColDocData: any
}, logJsonPayload: { operation: string, uid: string }) {
	const { colName, colDocUID, subColName, subColDocUID, subColDocData } = parameters;
	logger.info(`${colName}/${colDocUID}/${subColName}/${subColDocUID}/ created. DATA:: 
            ${JSON.stringify(subColDocData)}`, logJsonPayload);
}

export function logOnCollectionDocumentUpdate(parameters: {
	colName: string, colDocUID: string,
	colDocDataBefore: any, colDocDataAfter: any
}, logJsonPayload: { operation: string, uid: string }) {
	const { colName, colDocUID, colDocDataBefore, colDocDataAfter } = parameters;
	logger.info(`${colName}/${colDocUID} updated. BEFORE:: ${JSON.stringify(colDocDataBefore)}, AFTER:,
          ${JSON.stringify(colDocDataAfter)}`, logJsonPayload);
}

export function logOnSubCollectionDocumentUpdate(parameters: {
	colName: string, colDocUID: string, subColName: string,
	subColDocUID: string, subColDocDataBefore: any, subColDocDataAfter: any
}, logJsonPayload: { operation: string, uid: string }) {
	const { colName, colDocUID, subColName, subColDocUID, subColDocDataBefore, subColDocDataAfter } = parameters;
	logger.info(`${colName}/${colDocUID}/${subColName}/${subColDocUID}/ updated. BEFORE:: ${JSON.stringify(subColDocDataBefore)} 
          AFTER:: ${JSON.stringify(subColDocDataAfter)}`, logJsonPayload);
}

export function logOnFirestoreTriggerError(parameters: { functionName: string, message: string, error: Error },
	logJsonPayload: { operation: string, uid?: string }) {
	const { functionName, message, error } = parameters;
	logger.error(`Error:::: ${functionName}::${message}:: ${JSON.stringify(error)}`, error, logJsonPayload);
}

export function logOnCollectionDocumentSoftDelete(parameters: {
	colName: string, colDocUID: string,
	colDocDataBefore: any, colDocDataAfter: any
}, logJsonPayload: { operation: string, uid: string }) {
	const { colName, colDocUID, colDocDataBefore, colDocDataAfter } = parameters;
	logger.warn(`${colName}/${colDocUID} was soft deleted. BEFORE:: ${JSON.stringify(colDocDataBefore)}, AFTER:,
          ${JSON.stringify(colDocDataAfter)}`, logJsonPayload);
}

export function logOnSubCollectionDocumentSoftDelete(parameters: {
	colName: string, colDocUID: string, subColName: string,
	subColDocUID: string, subColDocDataBefore: any, subColDocDataAfter: any
}, logJsonPayload: { operation: string, uid: string }) {
	const { colName, colDocUID, subColName, subColDocUID, subColDocDataBefore, subColDocDataAfter } = parameters;
	logger.info(`${colName}/${colDocUID}/${subColName}/${subColDocUID}/ was soft deleted. BEFORE:: ${JSON.stringify(subColDocDataBefore)} 
          AFTER:: ${JSON.stringify(subColDocDataAfter)}`, logJsonPayload);
}

export function logOnScheduleFunctionError(parameters: { functionName: string, message: string, error: Error },) {
	const { functionName, message, error } = parameters;
	logger.error(`Error:::: ${functionName}::${message}:: ${JSON.stringify(error)}`, error,);
}

export function logOnFirestoreDocumentDoesNotExistError(parameters: { functionName: string, message: string, },
	logJsonPayload: { operation: string, uid: string }) {
	const { functionName, message } = parameters;
	logger.error(`Error:::: ${functionName}::${message}::`, logJsonPayload);
}

export function logOnFirestoreDocumentGetError(parameters: { functionName: string, message: string, error: Error },
	logJsonPayload: { operation: string, uid: string }) {
	const { functionName, message, error } = parameters;
	logger.error(`Error:::: ${functionName}::${message}:: ${JSON.stringify(error)}`, error, logJsonPayload);
}

export function logOnFirestoreDocumentUpdateSuccess(parameters: { functionName: string, message: string, },
	logJsonPayload: { operation: string, uid: string }) {
	const { functionName, message } = parameters;
	logger.info(`Success:::: ${functionName}::${message}`, logJsonPayload);
}

export function logOnFirestoreDocumentUpdateError(parameters: { functionName: string, message: string, error: Error },
	logJsonPayload: { operation: string, uid: string }) {
	const { functionName, message, error } = parameters;
	logger.error(`Error:::: ${functionName}::${message}:: ${JSON.stringify(error)}`, error, logJsonPayload);
}

export function logOnFirestoreDocumentDeleteError(parameters: { functionName: string, message: string, error: Error },
	logJsonPayload: { operation: string, uid: string }) {
	const { functionName, message, error } = parameters;
	logger.error(`Error:::: ${functionName}::${message}:: ${JSON.stringify(error)}`, error, logJsonPayload);
}

export function logOnFirestoreCollectionQueryError(parameters: { functionName: string, message: string, error: Error },
	logJsonPayload: { operation: string, queryParams: any }) {
	const { functionName, message, error } = parameters;
	logger.error(`Error:::: ${functionName}::${message}:: ${JSON.stringify(error)}
    :: QueryParams::${JSON.stringify(logJsonPayload.queryParams)}`, logJsonPayload);
}