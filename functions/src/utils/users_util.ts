import { collectionNames, subCollectionNames } from "./externalized_variables.util";
import {
    getGenericCollectionDocumentRef,
    getGenericSubCollectionDocumentRef,
} from "./collection_functions.util";
import {
    logOnFirestoreDocumentUpdateError,
    logOnFirestoreTriggerError,
} from './logger.util';

export async function updateUserDocument(userUID: string, data: FirebaseFirestore.DocumentData) {
    const userDocRef = getGenericCollectionDocumentRef(
        { colName: collectionNames.users, colDocUID: userUID }
    )
    return await userDocRef.update(data);
}

export async function updateProviderDetailsDocument(userUID: string, data: FirebaseFirestore.DocumentData) {
    const providerDetailDocRef = getGenericSubCollectionDocumentRef(
        { colName: collectionNames.users, colDocUID: userUID, subColName: subCollectionNames.providerDetails, subColDocUID: userUID }
    )
    return await providerDetailDocRef.update(data);
}

export async function operateOnLocations(providerDetailDocDataData: FirebaseFirestore.DocumentData) {
    const userUID = providerDetailDocDataData.uid;
    try {
        const { locations } = providerDetailDocDataData;
        const locationCountyLongNames: any[] = [];
        locations.forEach((location: any) => {
            const countyComponent = location.address_components.find((add: any) =>
                add.types.includes('administrative_area_level_2'));
            if (countyComponent && countyComponent['long_name']) {
                locationCountyLongNames.push(countyComponent['long_name'])
            }
        })
        const uniqueLocationCountyLongNames = [...new Set(locationCountyLongNames)];
        let toUpdateUserData: { locationCountyLongNames: string[] };
        if (uniqueLocationCountyLongNames && uniqueLocationCountyLongNames.length) {
            toUpdateUserData = {
                locationCountyLongNames: uniqueLocationCountyLongNames,
            }
        } else {
            toUpdateUserData = {
                locationCountyLongNames: [],
            }
        }
        try {
            await updateProviderDetailsDocument(userUID, toUpdateUserData);
            console.log('Success on updating locationCountyLongNames', locationCountyLongNames)
        }
        catch (error) {
            // we retry 
            logOnFirestoreDocumentUpdateError({
                functionName: 'operateOnLocations',
                message: "Operating on locations of providerDetail failed. We are retrying the update.",
                error,
            }, {
                operation: `${collectionNames.users}-update-error`, uid: userUID,
            })
            await updateProviderDetailsDocument(userUID, toUpdateUserData);
        }
    } catch (error) {
        logOnFirestoreTriggerError({
            functionName: 'operateOnLocations::queryGenericCollection',
            message: "Error while operating on provider detail locations.",
            error,
        }, {
            operation: `${collectionNames.users}-error`,
        })
    }
}