import { firestoreDatabase } from '../config/index';

/** 
 * Returns the collection reference 
 */
export function getGenericCollectionRef(parameters: { colName: string }) {
	const { colName } = parameters;
	return firestoreDatabase.collection(colName);
}

/** 
 * Returns the document reference under collection  
 */
export function getGenericCollectionDocumentRef(parameters: { colName: string, colDocUID: string }) {
	const { colName, colDocUID } = parameters;
	return firestoreDatabase.collection(colName)
		.doc(colDocUID);
}

/** 
 * Returns the nested subcollection reference 
 */
export function getGenericSubCollectionRef(parameters: { colName: string, colDocUID: string, subColName: string }) {
	const { colName, colDocUID, subColName } = parameters;
	return firestoreDatabase.collection(colName).doc(colDocUID)
		.collection(subColName);
}

/** 
 * Returns the document reference under nested subcollection
 */
export function getGenericSubCollectionDocumentRef(parameters: { colName: string, colDocUID: string, subColName: string, subColDocUID: string }) {
	const { colName, colDocUID, subColName, subColDocUID } = parameters;
	return firestoreDatabase.collection(colName).doc(colDocUID)
		.collection(subColName).doc(subColDocUID);
}

export function collectionDocumentFieldDataChanged(fieldValueBefore: any, fieldValueAfter: any) {
	return (fieldValueBefore !== fieldValueAfter);
}

export function isValidStringValue(fieldValue: string) {
	return (fieldValue !== null && fieldValue !== '');
}
