import { userRoleNames } from "./externalized_variables.util";

export function isRegisteredUser(currentUserRoles: Array<string> = []) {
    return currentUserRoles.includes(userRoleNames.registeredUser);
}

export function isProviderUser(currentUserRoles: Array<string> = []) {
    return currentUserRoles.includes(userRoleNames.providerUser);
}
