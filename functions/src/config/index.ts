

export import functions = require('firebase-functions');
export import firebaseAdmin = require('firebase-admin');
firebaseAdmin.initializeApp();
export const cors = require('cors')({ origin: true });
export const firestoreDatabase = firebaseAdmin.firestore();
export const config = functions.config();
export const logger = functions.logger;
