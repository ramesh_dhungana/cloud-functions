import { functions } from "../../config/index";
import { sendEmailToAdminOnNewAdminCreation } from "../../utils/admin.util";
import { setAdminCustomClaims } from "../../utils/custom_claims.util";
import { collectionNames, userRoleNames } from "../../utils/externalized_variables.util";
import { logOnCollectionDocumentCreate, logSimpleMessage } from "../../utils/logger.util";

export const onCreate = functions.firestore
	.document(`${collectionNames.admins}/{userDocUID}`)
	.onCreate(async (snap, context) => {
		const userDocData = snap.data();
		const { userDocUID } = context.params;
		logOnCollectionDocumentCreate(
			{ colName: collectionNames.admins, colDocUID: userDocUID, colDocData: userDocData },
			{
				operation: `${collectionNames.users}-create`, uid: userDocUID,
			}
		);
		try {
			await setAdminCustomClaims(userDocUID, userRoleNames.buzzarrAdmin);
			sendEmailToAdminOnNewAdminCreation(userDocUID, 'create');
		}
		catch (error) {
			logSimpleMessage(`Error on setUserRolesCustomClaims(${userDocUID},${userRoleNames.buzzarrAdmin}):: ${error}`);
		}
		return null;
	});

