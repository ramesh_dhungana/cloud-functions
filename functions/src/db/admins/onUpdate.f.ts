import { functions } from "../../config/index";
import { sendEmailToAdminOnNewAdminCreation } from "../../utils/admin.util";
import {
	setAdminCustomClaims,
} from "../../utils/custom_claims.util";
import {
	collectionNames,
	userRoleNames,
} from "../../utils/externalized_variables.util";
import {
	logOnCollectionDocumentUpdate,
	logSimpleMessage,
} from "../../utils/logger.util";

export const onUpdate = functions.firestore
	.document(`${collectionNames.admins}/{userDocUID}`)
	.onUpdate(async (change, context) => {
		const userDocDataBefore = change.before.data();
		const userDocDataAfter = change.after.data();
		const { userDocUID } = context.params;
		logOnCollectionDocumentUpdate({
			colName: collectionNames.admins, colDocUID: userDocUID,
			colDocDataBefore: userDocDataBefore, colDocDataAfter: userDocDataAfter,
		}, {
			operation: `${collectionNames.admins}-update`, uid: userDocUID,
		});
		try {
			await setAdminCustomClaims(userDocUID, userRoleNames.buzzarrAdmin);
			sendEmailToAdminOnNewAdminCreation(userDocUID,'update');
		}
		catch (error) {
			logSimpleMessage(`Error on setUserRolesCustomClaims(${userDocUID},${userRoleNames.buzzarrAdmin}):: ${error}`);
		}
		return null;
	});