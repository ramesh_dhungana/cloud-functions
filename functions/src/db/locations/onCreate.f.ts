import { functions } from "../../config/index";
import { collectionNames } from "../../utils/externalized_variables.util";
import {
	logOnCollectionDocumentCreate,
} from "../../utils/logger.util";

export const onCreate = functions.firestore
	.document(`${collectionNames.locations}/{locationDocUID}`)
	.onCreate(async (snap, context) => {
		const locationDocData = snap.data();
		const { locationDocUID } = context.params;
		logOnCollectionDocumentCreate(
			{ colName: collectionNames.locations, colDocUID: locationDocUID, colDocData: locationDocData },
			{
				operation: `${collectionNames.locations}-create`, uid: locationDocUID,
			}
		);
		return null;
	});

