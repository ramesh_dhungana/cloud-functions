import { functions } from "../../config/index";
import { collectionDocumentFieldDataChanged } from "../../utils/collection_functions.util";
import {
	collectionNames, userAccountStatus, locationStatus,
} from "../../utils/externalized_variables.util";
import {
	queryGenericCollection,
	updateCollectionDoc,
} from "../../utils/generic-collection-data.util";
import {
	logOnCollectionDocumentUpdate,
	logOnFirestoreCollectionQueryError,
	logOnFirestoreDocumentUpdateError,
	logOnFirestoreDocumentUpdateSuccess,
	logOnFirestoreTriggerError,
	logOperationsType,
} from "../../utils/logger.util";

export const onUpdate = functions.firestore
	.document(`${collectionNames.locations}/{locationDocUID}`)
	.onUpdate(async (change, context) => {
		const locationDocDataBefore = change.before.data();
		const locationDocDataAfter = change.after.data();
		const { locationDocUID } = context.params;
		logOnCollectionDocumentUpdate({
			colName: collectionNames.locations, colDocUID: locationDocUID,
			colDocDataBefore: locationDocDataBefore, colDocDataAfter: locationDocDataAfter,
		}, {
			operation: `${collectionNames.locations}-update`, uid: locationDocUID,
		});
		if (locationIsEnabledAndApproved(locationDocDataBefore, locationDocDataAfter) ||
			locationIsApprovedAndReEnabled(locationDocDataBefore, locationDocDataAfter)) {
			logOnFirestoreDocumentUpdateSuccess({
				functionName: 'locationIsEnabledAndApproved',
				message: "Location is approved and enabled, thus we are activating on hold users of this location county.",
			}, {
				operation: logOperationsType.locationsEnabled, uid: locationDocUID,
			});
			activateOnHoldUsersAssociatedWithLocation(locationDocDataAfter);
		};
		if (locationIsApprovedAndDisabled(locationDocDataBefore, locationDocDataAfter)) {
			logOnFirestoreDocumentUpdateSuccess({
				functionName: 'locationIsApprovedAndDisabled',
				message: "Location is approved and disabled, thus we are keeping users of this location county on hold users.",
			}, {
				operation: logOperationsType.locationsDisabled, uid: locationDocUID,
			});
			keepOnholdToActiveUsersAssociatedWithDisabledLocation(locationDocDataAfter);
		}
	})


function locationIsEnabledAndApproved(locationDocDataBefore: FirebaseFirestore.DocumentData,
	locationDocDataAfter: FirebaseFirestore.DocumentData) {
	const fieldChanged = collectionDocumentFieldDataChanged(locationDocDataBefore.status, locationDocDataAfter.status);
	const isApproved = locationDocDataAfter?.status === locationStatus.approved;
	const isEnabled = locationDocDataAfter?.disabled === false;
	const result = fieldChanged && isApproved && isEnabled;
	return result;
}

function locationIsApprovedAndReEnabled(locationDocDataBefore: FirebaseFirestore.DocumentData,
	locationDocDataAfter: FirebaseFirestore.DocumentData) {
	const fieldChanged = collectionDocumentFieldDataChanged(locationDocDataBefore.disabled, locationDocDataAfter.disabled);
	const isApproved = locationDocDataAfter?.status === locationStatus.approved;
	const isEnabled = locationDocDataAfter?.disabled === false;
	const result = fieldChanged && isApproved && isEnabled;
	return result;
}

function activateOnHoldUsersAssociatedWithLocation(locationDocDataAfter: FirebaseFirestore.DocumentData) {
	try {
		const { location } = locationDocDataAfter;
		const locationCountyComponent = location.address_components.find((add: any) =>
			add.types.includes('administrative_area_level_2'));
		const userParams = {
			colName: collectionNames.users,
			queryList: [
				{ key: 'locationCountyLongName', operator: '==', value: locationCountyComponent['long_name'] },
				{ key: 'account_status', operator: '==', value: userAccountStatus.hold },
			],
		}
		queryGenericCollection(userParams)
			.then(ohHoldUserSnaps => {
				ohHoldUserSnaps.forEach(async userSnap => {
					const userUID = userSnap.id;
					const userData = userSnap.data();
					console.log(`We are activating user with uid::, ${userUID}, email:: ${userData.email}`);
					const toUpdateData = { account_status: userAccountStatus.active };
					const updateParams = {
						colName: collectionNames.users,
						colDocUID: userUID, colDocData: toUpdateData,
					}
					updateCollectionDoc(updateParams)
						.catch(error => {
							logOnFirestoreDocumentUpdateError({
								functionName: 'updateCollectionDoc',
								message: "Error while updating active users' account_status field to active.",
								error,
							}, {
								operation: `${collectionNames.users}-update-error`, uid: userUID,
							})
						})
				})
			})
			.catch(error => {
				logOnFirestoreCollectionQueryError({
					functionName: 'queryGenericCollection',
					message: "Error while fetching on hold users of given location county.",
					error: error,
				}, {
					operation: `${collectionNames.users}-query-error`,
					queryParams: userParams,
				})
			})
	} catch (error) {
		logOnFirestoreTriggerError({
			functionName: 'activeOnHoldUsersAssociatedWithLocation::queryGenericCollection',
			message: "Error while querying on hold users for given location",
			error,
		}, {
			operation: `${collectionNames.users}-error`,
		})
	}
}

function locationIsApprovedAndDisabled(locationDocDataBefore: FirebaseFirestore.DocumentData,
	locationDocDataAfter: FirebaseFirestore.DocumentData) {
	const fieldChanged = collectionDocumentFieldDataChanged(locationDocDataBefore.disabled, locationDocDataAfter.disabled);
	const isApproved = locationDocDataAfter?.status === locationStatus.approved;
	const isDisabled = locationDocDataAfter?.disabled === true;
	const result = fieldChanged && isApproved && isDisabled;
	return result;
}

function keepOnholdToActiveUsersAssociatedWithDisabledLocation(locationDocDataAfter: FirebaseFirestore.DocumentData) {
	try {
		const { location } = locationDocDataAfter;
		const locationCountyComponent = location.address_components.find((add: any) =>
			add.types.includes('administrative_area_level_2'));
		const userParams = {
			colName: collectionNames.users,
			queryList: [
				{ key: 'locationCountyLongName', operator: '==', value: locationCountyComponent['long_name'] },
				{ key: 'account_status', operator: '==', value: userAccountStatus.active },
			],
		}
		queryGenericCollection(userParams)
			.then(activeUserSnaps => {
				activeUserSnaps.forEach(async userSnap => {
					const userUID = userSnap.id;
					const userData = userSnap.data();
					console.log(`We are keeping user with uid::, ${userUID}, email:: ${userData.email} on hold as location has been disabled.`);
					const toUpdateData = { account_status: userAccountStatus.hold };
					const updateParams = {
						colName: collectionNames.users,
						colDocUID: userUID, colDocData: toUpdateData,
					}
					updateCollectionDoc(updateParams)
						.catch(error => {
							logOnFirestoreDocumentUpdateError({
								functionName: 'updateCollectionDoc',
								message: "Error while updating active users' account_status field to hold.",
								error,
							}, {
								operation: `${collectionNames.users}-update-error`, uid: userUID,
							})
						})
				})
			})
			.catch(error => {
				logOnFirestoreCollectionQueryError({
					functionName: 'queryGenericCollection',
					message: "Error while fetching on active users of given location county.",
					error: error,
				}, {
					operation: `${collectionNames.users}-query-error`,
					queryParams: userParams,
				})
			})
	} catch (error) {
		logOnFirestoreTriggerError({
			functionName: 'activeOnHoldUsersAssociatedWithLocation::queryGenericCollection',
			message: "Error while querying on active users for given location",
			error,
		}, {
			operation: `${collectionNames.users}-error`,
		})
	}
}