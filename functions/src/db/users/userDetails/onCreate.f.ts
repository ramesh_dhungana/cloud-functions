import { functions } from '../../../config/index';
import { collectionNames, subCollectionNames } from "../../../utils/externalized_variables.util";
import { logOnSubCollectionDocumentCreate } from "../../../utils/logger.util";

export const onUserDetailsCreate = functions.firestore
    .document(`${collectionNames.users}/{userDocUID}/${subCollectionNames.userDetails}/{userDetailDocUID}`)
    .onCreate((snap, context) => {
        const userDetailDocData = snap.data();
        const { userDocUID, userDetailDocUID } = context.params;
        logOnSubCollectionDocumentCreate({
            colName: collectionNames.users, colDocUID: userDocUID, subColName: subCollectionNames.userDetails,
            subColDocUID: userDetailDocUID, subColDocData: userDetailDocData,
        }, {
            operation: `${subCollectionNames.userDetails}-create`, uid: userDocUID,
        });
    });
