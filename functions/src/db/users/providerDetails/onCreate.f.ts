import { functions } from '../../../config/index';
import {
    collectionNames,
    subCollectionNames,
    userRoleNames,
} from "../../../utils/externalized_variables.util";
import { setUserRolesCustomClaims } from "../../../utils/custom_claims.util";
import { logOnSubCollectionDocumentCreate } from "../../../utils/logger.util";
import { operateOnLocations } from '../../../utils/users_util';

export const onProviderDetailsCreate = functions.firestore
    .document(`${collectionNames.users}/{userDocUID}/${subCollectionNames.providerDetails}/{providerDetailDocUID}`)
    .onCreate(async (snap, context) => {
        const providerDetailDocData = snap.data();
        const { userDocUID, providerDetailDocUID } = context.params;
        logOnSubCollectionDocumentCreate({
            colName: collectionNames.users, colDocUID: userDocUID, subColName: subCollectionNames.providerDetails,
            subColDocUID: providerDetailDocUID, subColDocData: providerDetailDocData,
        }, {
            operation: `${subCollectionNames.providerDetails}-create`, uid: userDocUID,
        });
        const roleName = userRoleNames.providerUser;
        setUserRolesCustomClaims(userDocUID, roleName)
            .then(response => {
                console.log()
            }).catch(error => { console.log() });
        await operateOnLocations(providerDetailDocData);
        return null;
    });
