import { functions } from '../../../config/index';
import { collectionDocumentFieldDataChanged } from '../../../utils/collection_functions.util';
import { collectionNames, subCollectionNames } from "../../../utils/externalized_variables.util";
import { logOnSubCollectionDocumentUpdate } from '../../../utils/logger.util';
import { operateOnLocations } from '../../../utils/users_util';

export const onProviderDetailsUpdate = functions.firestore
    .document(`${collectionNames.users}/{userDocUID}/${subCollectionNames.providerDetails}/{providerDetailDocUID}`)
    .onUpdate(async (change, context) => {
        const providerDetailDocDataBefore = change.before.data();
        const providerDetailDocDataDataAfter = change.after.data();
        const { userDocUID } = context.params;
        logOnSubCollectionDocumentUpdate({
            colName: collectionNames.users, colDocUID: userDocUID, subColName: subCollectionNames.providerDetails,
            subColDocUID: userDocUID, subColDocDataBefore: providerDetailDocDataBefore,
            subColDocDataAfter: providerDetailDocDataDataAfter,
        }, {
            operation: `${subCollectionNames.providerDetails}-update`, uid: userDocUID,
        })
        if (collectionDocumentFieldDataChanged(providerDetailDocDataBefore?.locations,
            providerDetailDocDataDataAfter?.locations)) {
            await operateOnLocations(providerDetailDocDataDataAfter);
        }
        return null;
    });

