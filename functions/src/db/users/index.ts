export { onUserDetailsCreate } from "./userDetails/onCreate.f";
export { onProviderDetailsCreate } from "./providerDetails/onCreate.f";
export { onProviderDetailsUpdate } from "./providerDetails/onUpdate.f";
export { onCreate } from "./onCreate.f";
export { onUpdate } from "./onUpdate.f";
