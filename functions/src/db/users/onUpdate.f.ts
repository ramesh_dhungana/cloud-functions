import { isEmpty, xor } from "lodash";
import { functions, config } from "../../config/index";
import { collectionDocumentFieldDataChanged } from "../../utils/collection_functions.util";
import {
	setRegistrationIsCompletedCustomClaims,
	setUserRolesCustomClaims,
} from "../../utils/custom_claims.util";
import {
	collectionNames, genericVariableNames,
	userRoleNames,
	userAccountStatus,
	proposalStatus,
	quoteStatus,
	environmentNames,
	locationStatus,
} from "../../utils/externalized_variables.util";
import { queryGenericCollection, updateCollectionDoc } from "../../utils/generic-collection-data.util";
import {
	logOnCollectionDocumentSoftDelete,
	logOnCollectionDocumentUpdate,
	logOnFirestoreDocumentUpdateError,
	logOnFirestoreCollectionQueryError,
	logOnFirestoreDocumentUpdateSuccess,
	logOperationsType,
	logSimpleMessage,
} from "../../utils/logger.util";
import { isRegisteredUser, isProviderUser } from "../../utils/role_functions.utils";
import { disableAuthUser, enableAuthUser } from '../../utils/auth_user_util';
import { sendCustomEmail, isValidBuzzarrEmailDomain } from '../../utils/custom_email.util';
import { updateUserDocument } from '../../utils/users_util';
import { accountStatusChangedEmailTemplate } from '../../email-templates/account_status_changed';
import { welcomeToBuzzarrEmailTemplate } from '../../email-templates/welcome_to_buzzarr';

const limitToEmailAdminOnUserFlaggedByUser = 5;
const thresholdToFlagUserFlaggedByUsers = 10;

export const onUpdate = functions.firestore
	.document(`${collectionNames.users}/{userDocUID}`)
	.onUpdate(async (change, context) => {
		const userDocDataBefore = change.before.data();
		const userDocDataAfter = change.after.data();
		const { userDocUID } = context.params;
		logOnCollectionDocumentUpdate({
			colName: collectionNames.users, colDocUID: userDocUID,
			colDocDataBefore: userDocDataBefore, colDocDataAfter: userDocDataAfter,
		}, {
			operation: `${collectionNames.users}-update`, uid: userDocUID,
		});

		if (photoURLGenerationIsRequired(userDocDataBefore, userDocDataAfter)) {
			const photoURL = generateUserAvatarPhotoURL(userDocDataAfter);
			await updateUserDocument(userDocUID, { photoURL: photoURL });
		}
		if (config.envvar.environment === environmentNames.production) {
			// we here determine if the user's location is active or not and set account_status on hold or active
			await determineIfUserLocationIsInBuzzarrOperatingRegion(userDocDataBefore, userDocDataAfter);
		} else {
			if (registrationIsCompletedFieldChanged(userDocDataBefore, userDocDataAfter)) {
				const { location } = userDocDataAfter;
				const userLocationCountyComponent = location.address_components.find((add: any) =>
					add.types.includes('administrative_area_level_2'));
				const locationCountyLongName = userLocationCountyComponent ? userLocationCountyComponent['long_name'] : '';
				const isBuzzarrEmail = isValidBuzzarrEmailDomain(userDocDataAfter.email);
				if (isBuzzarrEmail) {
					await determineIfUserLocationIsInBuzzarrOperatingRegion(userDocDataBefore, userDocDataAfter);
				} else {
					const toUpdateUserData = {
						account_status: userAccountStatus.disabled,
						locationCountyLongName,
					}
					try {
						await updateUserDocument(userDocUID, toUpdateUserData);
					}
					catch (error) {
						logOnFirestoreDocumentUpdateError({
							functionName: 'updateUserDocument',
							message: "Updating user account_status to on disabled for dev  and staging environment. We are retrying the update.",
							error,
						}, {
							operation: `${collectionNames.users}-update-error`, uid: userDocUID,
						})
						await updateUserDocument(userDocUID, toUpdateUserData)
					}
				}
			}
		}

		// we add custom claims only once, ie when given role is added first time
		if (userRoleIsChanged(userDocDataBefore, userDocDataAfter)) {
			if (isProviderUserRoleAddedFirstTime(userDocDataBefore, userDocDataAfter)) {
				const roleName = userRoleNames.providerUser;
				setUserRolesCustomClaims(userDocUID, roleName)
					.then(response => {
						console.log()
					}).catch(error => { console.log() });;
			}
			if (isRegisteredUserRoleAddedFirstTime(userDocDataBefore, userDocDataAfter)) {
				const roleName = userRoleNames.registeredUser;
				setUserRolesCustomClaims(userDocUID, roleName)
					.then(response => {
						console.log()
					}).catch(error => { console.log() });
			}
		}

		// we set the registrationIsCompleted custom  claims once registration process is completed
		if (registrationIsCompletedFieldChanged(userDocDataBefore, userDocDataAfter)) {
			console.log(`registrationIsCompleted field changed, BEFORE:: ${userDocDataBefore[genericVariableNames.registrationIsCompleted]},
             AFTER:: ${userDocDataAfter[genericVariableNames.registrationIsCompleted]}.`)
			if (userDocDataAfter[genericVariableNames.registrationIsCompleted] === true) {
				await sendUserWelcomeEmailTo(userDocDataAfter);
				setRegistrationIsCompletedCustomClaims(userDocUID, !!userDocDataAfter[genericVariableNames.registrationIsCompleted])
					.then(response => {
						console.log(`Success: setRegistrationIsCompletedCustomClaims(${userDocUID}, ${userDocDataAfter[genericVariableNames.registrationIsCompleted]}`)
					}).catch(error => {
						console.log(`Error: setRegistrationIsCompletedCustomClaims(${userDocUID}, ${userDocDataAfter[genericVariableNames.registrationIsCompleted]}:: ${error}`)
					});
			}
		}

		/*perform all activities after user account is deleted: 
		1. disable the auth user.
		2. all proposals by user changed to archived.
		3. all quotes by user changed to archived.
		*/
		if (userAccountStatusHasChanged(userDocDataBefore, userDocDataAfter)) {
			logOnFirestoreDocumentUpdateSuccess({
				functionName: 'userAccountStatusHasChanged',
				message: "User account_status was changed.",
			}, {
				operation: logOperationsType.usersAccountStatusChanged, uid: userDocUID,
			});
			await performCheckOnDifferentAccountStatusChange(userDocDataBefore, userDocDataAfter)
		}
		if (proposalUserFlagsChanged(userDocDataBefore, userDocDataAfter)
		) {
			const { userFlags } = userDocDataAfter;
			const userFlagsCountAfter: number = userFlags?.length;
			switch (userFlagsCountAfter) {
				case limitToEmailAdminOnUserFlaggedByUser:
					await sendEmailToAdminOnUserFlagsOnUserReachedLimit(userDocDataAfter)
					break;
				case thresholdToFlagUserFlaggedByUsers:
					await flagUserAfterUserFlaggedReachedThreshold(userDocDataAfter);
					break;
			}
			const userFlagsCountBefore = userDocDataBefore?.userFlags?.length;
			if (userFlagsCountBefore >= thresholdToFlagUserFlaggedByUsers
				&& userFlagsCountAfter < thresholdToFlagUserFlaggedByUsers) {
				const updateParams = {
					colName: collectionNames.users,
					colDocUID: userDocUID,
					colDocData: { account_status: userAccountStatus.active },
				}
				try {
					await updateCollectionDoc(updateParams);
				} catch (error) {
					logOnFirestoreDocumentUpdateError({
						functionName: 'updateCollectionDoc',
						message: "Error while updating flagged user proposal after count of userFlags is less then threshold.",
						error,
					}, {
						operation: `${collectionNames.users}-update-error`, uid: userDocUID,
					})
				}
			}
		}
		return null;
	});

async function determineIfUserLocationIsInBuzzarrOperatingRegion(userDocDataBefore: FirebaseFirestore.DocumentData,
	userDocDataAfter: FirebaseFirestore.DocumentData) {
	if (userDocDataBefore?.location?.formatted_address !== userDocDataAfter?.location?.formatted_address) {
		const userUID = userDocDataAfter.uid;
		const locationParams = {
			colName: collectionNames.locations,
			queryList: [
				{ key: 'disabled', operator: '==', value: false },
				{ key: 'status', operator: '==', value: locationStatus.approved },
			],
		}
		try {
			const activeLocationSnaps = await queryGenericCollection(locationParams);
			const activeLocations: FirebaseFirestore.DocumentData[] = [];
			activeLocationSnaps.forEach(async locationSnap => {
				activeLocations.push(locationSnap.data());
			});
			const { location } = userDocDataAfter;
			const userLocationCountyComponent = location.address_components.find((add: any) =>
				add.types.includes('administrative_area_level_2'));
			const locationMatched = activeLocations.find(locDoc => {
				const countyComponent = locDoc.location.address_components.find((add: any) =>
					add.types.includes('administrative_area_level_2'));
				if (countyComponent && userLocationCountyComponent
					&& countyComponent['long_name'] === userLocationCountyComponent['long_name']) {
					return locDoc;
				} else {
					return null;
				}
			})
			console.log('locationMatched', locationMatched)
			let toUpdateUserData: { account_status: string, locationCountyLongName: string };
			const locationCountyLongName = userLocationCountyComponent ? userLocationCountyComponent['long_name'] : ''
			if (locationMatched) {
				toUpdateUserData = {
					account_status: userAccountStatus.active,
					locationCountyLongName,
				}
			} else {
				toUpdateUserData = {
					account_status: userAccountStatus.hold,
					locationCountyLongName,
				}
			}
			try {
				await updateUserDocument(userUID, toUpdateUserData);
			}
			catch (error) {
				// we retry 
				logOnFirestoreDocumentUpdateError({
					functionName: 'determineIfUserLocationIsInBuzzarrOperatingRegion',
					message: "Updating user account_status after location filter failed. We are retrying the update.",
					error,
				}, {
					operation: `${collectionNames.users}-update-error`, uid: userUID,
				})
				await updateUserDocument(userUID, toUpdateUserData)
			}
		} catch (error) {
			logOnFirestoreCollectionQueryError({
				functionName: 'queryGenericCollection',
				message: "Error while fetching active locations user.",
				error,
			}, {
				operation: `${collectionNames.users}-query-error`, queryParams: locationParams,
			})
		}
	}
}

function registrationIsCompletedFieldChanged(docDataBefore: FirebaseFirestore.DocumentData,
	docDataAfter: FirebaseFirestore.DocumentData) {
	const fieldChanged = collectionDocumentFieldDataChanged(docDataBefore.registrationIsCompleted, docDataAfter.registrationIsCompleted);
	logSimpleMessage(`registrationIsCompletedFieldChanged:: ${fieldChanged}`);
	return fieldChanged;
}

function userRoleIsChanged(docDataBefore: FirebaseFirestore.DocumentData,
	docDataAfter: FirebaseFirestore.DocumentData) {
	return (collectionDocumentFieldDataChanged(docDataBefore.roles, docDataAfter.roles)
		&& isEmpty(xor(docDataBefore.roles, docDataAfter.roles)) && docDataAfter.roles.length > 0);
}


function isProviderUserRoleAddedFirstTime(docDataBefore: FirebaseFirestore.DocumentData,
	docDataAfter: FirebaseFirestore.DocumentData) {
	if (docDataBefore.roles && docDataAfter.roles) {
		return (!isProviderUser(docDataBefore.roles) && isProviderUser(docDataAfter.roles))

	} else {
		return isProviderUser(docDataAfter.roles);
	}
};

function isRegisteredUserRoleAddedFirstTime(docDataBefore: FirebaseFirestore.DocumentData,
	docDataAfter: FirebaseFirestore.DocumentData) {
	if (docDataBefore.roles && docDataAfter.roles) {
		return (!isRegisteredUser(docDataBefore.roles) && isRegisteredUser(docDataAfter.roles))

	} else {
		return isRegisteredUser(docDataAfter.roles);
	}
}

async function archiveAllProposalsOfDeletedUser(userUID: string): Promise<void> {
	const proposalParams = {
		colName: collectionNames.proposals,
		queryList: [
			{ key: 'user', operator: '==', value: userUID },
		],
	}
	try {
		const allProposalSnaps = await queryGenericCollection(proposalParams);
		allProposalSnaps.forEach(async proposalSnap => {
			const proposalUID = proposalSnap.id;
			const toUpdateData = { status: proposalStatus.archived, updatedAt: new Date() };
			const updateParams = {
				colName: collectionNames.proposals,
				colDocUID: proposalUID,
				colDocData: toUpdateData,
			}
			try {
				await updateCollectionDoc(updateParams);
			} catch (error) {
				logOnFirestoreDocumentUpdateError({
					functionName: 'updateCollectionDoc',
					message: "Error while updating closed quotes' proposal status to archived.",
					error,
				}, {
					operation: `${collectionNames.users}-update-error`, uid: userUID,
				});
			}
		})
	} catch (error) {
		logOnFirestoreCollectionQueryError({
			functionName: 'queryGenericCollection',
			message: "Error while fetching proposals for deleted user.",
			error,
		}, {
			operation: `${collectionNames.users}-query-error`, queryParams: proposalParams,
		})
	}
}

async function archiveAllQuotesOfDeletedUser(userUID: string): Promise<void> {
	const quoteParams = {
		colName: collectionNames.quotes,
		queryList: [
			{ key: 'user', operator: '==', value: userUID },
		],
	}
	try {
		const allQuotesSnaps = await queryGenericCollection(quoteParams);
		allQuotesSnaps.forEach(async quoteSnap => {
			const quoteUID = quoteSnap.id;
			const toUpdateData = { status: quoteStatus.archived, updatedAt: new Date() };
			const updateParams = {
				colName: collectionNames.quotes,
				colDocUID: quoteUID,
				colDocData: toUpdateData,
			}
			try {
				await updateCollectionDoc(updateParams);
			} catch (error) {
				logOnFirestoreDocumentUpdateError({
					functionName: 'updateCollectionDoc',
					message: "Error while updating deleted user's quotes status to archived.",
					error,
				}, {
					operation: `${collectionNames.users}-update-error`, uid: userUID,
				})
			}
		})
	} catch (error) {
		logOnFirestoreCollectionQueryError({
			functionName: 'queryGenericCollection',
			message: "Error while fetching quotes for deleted user.",
			error,
		}, {
			operation: `${collectionNames.users}-update-error`, queryParams: quoteParams,
		})
	}
}

function userAccountStatusHasChanged(docDataBefore: FirebaseFirestore.DocumentData,
	docDataAfter: FirebaseFirestore.DocumentData) {
	const fieldChanged = collectionDocumentFieldDataChanged(docDataBefore.account_status,
		docDataAfter.account_status);
	return fieldChanged;
}

async function performCheckOnDifferentAccountStatusChange(userDocDataBefore: FirebaseFirestore.DocumentData,
	userDocDataAfter: FirebaseFirestore.DocumentData) {
	const { uid, email, firstName } = userDocDataAfter;
	const userDocUID = uid;
	if (userAccountStatusChangedToDeleted(userDocDataAfter)) {
		try {
			await disableAuthUser(userDocUID);
			await archiveAllProposalsOfDeletedUser(userDocUID);
			await archiveAllQuotesOfDeletedUser(userDocUID);
			const emailContent = `Your account has been deleted.
                If this was not you then, 
                please contact us at ${config.envvar.buzzarr_support_email}!`
			const mailData = {
				to: email,
				from: config.envvar.buzzarr_noreply_email,
				subject: 'Buzzarr | Your account has been deleted!',
				text: '.',
				html: accountStatusChangedEmailTemplate({ firstName, emailContent }),
			}
			await sendCustomEmail(mailData);
			console.log('Successfully disabled auth user:::', userDocUID);
			logOnCollectionDocumentSoftDelete({
				colName: collectionNames.users, colDocUID: userDocUID,
				colDocDataBefore: userDocDataBefore, colDocDataAfter: userDocDataAfter,
			}, {
				operation: `${collectionNames.users}-soft-delete`, uid: userDocUID,
			});

		} catch (error) {
			console.log('Error on userAccountStatusChangedToDeleted:::', error);
		}
	}
	if (userAccountStatusChangedToActive(userDocDataAfter)) {
		if (userAccountStatusChangedFromHoldToActive(userDocDataBefore, userDocDataAfter)) {
			const emailContent = `Your buzzarr account has been activated!
                Click the link below to get started using Buzzarr!`
			const mailData = {
				to: email,
				from: config.envvar.buzzarr_noreply_email,
				subject: 'Buzzarr | Your account has been activated!',
				text: '.',
				html: accountStatusChangedEmailTemplate({ firstName, emailContent }),
			}
			await sendCustomEmail(mailData);
		} else {
			const emailContent = `Your buzzarr account has been re-activated!
                Click the link below to get started using Buzzarr!`
			const mailData = {
				to: email,
				from: config.envvar.buzzarr_noreply_email,
				subject: 'Buzzarr | Your account has been re-activated!',
				text: '.',
				html: accountStatusChangedEmailTemplate({ firstName, emailContent }),
			}
			await sendCustomEmail(mailData);
		}
	}
	if (userAccountStatusChangedToHold(userDocDataAfter)) {
		const emailContent = `Buzzarr is expanding and we plan to operate in your area soon. 
                We will notify you by email when Buzzarr becomes available for you. 
                We look forward to serving you soon!`;
		const mailData = {
			to: email,
			from: config.envvar.buzzarr_noreply_email,
			subject: 'Buzzarr | Your account has been on hold!',
			text: '.',
			html: accountStatusChangedEmailTemplate({ firstName, emailContent }),
		}
		await sendCustomEmail(mailData);
	}
	if (userAccountStatusChangedToDisabled(userDocDataAfter)) {
		await disableAuthUser(userDocUID);
		const emailContent = `Your account has been disabled. 
                If you believe this was an error, 
                please contact us at ${config.envvar.buzzarr_support_email}!`;
		const mailData = {
			to: email,
			from: config.envvar.buzzarr_noreply_email,
			subject: 'Buzzarr | Your account has been disabled!',
			text: '.',
			html: accountStatusChangedEmailTemplate({ firstName, emailContent }),
		}
		await sendCustomEmail(mailData);
	}
	if (userAccountStatusChangedToFlagged(userDocDataAfter)) {
		const emailContent = `Your account has been flagged due to suspicious activity or potential misuse. 
                We will send you an email with any updates.
                If you believe this was an error, 
                please contact us at ${config.envvar.buzzarr_support_email}!`;
		const mailData = {
			to: email,
			from: config.envvar.buzzarr_noreply_email,
			subject: 'Buzzarr | Your account has been flagged!',
			text: '.',
			html: accountStatusChangedEmailTemplate({ firstName, emailContent }),
		}
		await sendCustomEmail(mailData);
	}

	if (reEnableUserOnAccountStatusChange(userDocDataBefore, userDocDataAfter)) {
		await enableAuthUser(userDocDataAfter.uid);
	}

}

function userAccountStatusChangedToDeleted(docDataAfter: FirebaseFirestore.DocumentData) {
	return docDataAfter.account_status === userAccountStatus.deleted;
}

function userAccountStatusChangedFromHoldToActive(docDataBefore: FirebaseFirestore.DocumentData,
	docDataAfter: FirebaseFirestore.DocumentData) {
	return docDataBefore.account_status === userAccountStatus.hold &&
		docDataAfter.account_status === userAccountStatus.active;
}

function userAccountStatusChangedToHold(docDataAfter: FirebaseFirestore.DocumentData) {
	return docDataAfter.account_status === userAccountStatus.hold;
}

function userAccountStatusChangedToDisabled(docDataAfter: FirebaseFirestore.DocumentData) {
	return docDataAfter.account_status === userAccountStatus.disabled;
}

function userAccountStatusChangedToFlagged(docDataAfter: FirebaseFirestore.DocumentData) {
	return docDataAfter.account_status === userAccountStatus.flagged;
}

function userAccountStatusChangedToActive(docDataAfter: FirebaseFirestore.DocumentData) {
	return docDataAfter.account_status === userAccountStatus.active;
}

function reEnableUserOnAccountStatusChange(docDataBefore: FirebaseFirestore.DocumentData,
	docDataAfter: FirebaseFirestore.DocumentData) {
	return (docDataBefore.account_status === userAccountStatus.deleted
		|| docDataBefore.account_status === userAccountStatus.disabled)
		&&
		(docDataAfter.account_status !== userAccountStatus.deleted
			&& docDataAfter.account_status !== userAccountStatus.disabled);
}

function photoURLGenerationIsRequired(docDataBefore: FirebaseFirestore.DocumentData,
	docDataAfter: FirebaseFirestore.DocumentData) {
	const firstNameChanged = collectionDocumentFieldDataChanged(docDataBefore.firstName, docDataAfter.firstName);
	const lastNameChanged = collectionDocumentFieldDataChanged(docDataBefore.lastName, docDataAfter.lastName);
	const photoURLIsFromUIAvatar = docDataBefore?.photoURL.includes("https://ui-avatars.com/api");
	return (firstNameChanged || lastNameChanged) && photoURLIsFromUIAvatar;
}

function generateUserAvatarPhotoURL(userDataAfter: FirebaseFirestore.DocumentData) {
	return `https://ui-avatars.com/api/?background=random&name=${userDataAfter.firstName}+${userDataAfter.lastName}&bold=${true}`;
}

function proposalUserFlagsChanged(userDocDataBefore: FirebaseFirestore.DocumentData,
	userDocDataAfter: FirebaseFirestore.DocumentData) {
	return collectionDocumentFieldDataChanged(userDocDataBefore.userFlags, userDocDataAfter.userFlags);
}

async function sendEmailToAdminOnUserFlagsOnUserReachedLimit(userData: FirebaseFirestore.DocumentData) {
	const mailData = {
		to: config.envvar.buzzarr_admin_email,
		from: config.envvar.buzzarr_alert_email,
		subject: `Buzzarr | User has been flagged ${limitToEmailAdminOnUserFlaggedByUser} times by users!`,
		text: '.',
		html: `
                <p>Hi,</p>
                <p>The user with following detail has been flagged ${limitToEmailAdminOnUserFlaggedByUser} times by users:</p>
                <p>
                ${JSON.stringify(userData)}
                </p>
                <p>Thus, the user account status has been flagged!</p>
                <p>Thanks,</p>
                <p><b>Your Buzzarr Team</b></p>`,
	}
	await sendCustomEmail(mailData);
}

async function flagUserAfterUserFlaggedReachedThreshold(userData: FirebaseFirestore.DocumentData) {
	const userDocUID = userData.uid;
	const updateParams = {
		colName: collectionNames.users,
		colDocUID: userDocUID,
		colDocData: { account_status: userAccountStatus.flagged },
	}
	try {
		await updateCollectionDoc(updateParams);
		await sendEmailToAdminWhenUserIsFlagged(userData);
	} catch (error) {
		logOnFirestoreDocumentUpdateError({
			functionName: 'updateCollectionDoc',
			message: "Error while flagging user after flagged multiple times by users.",
			error,
		}, {
			operation: `${collectionNames.users}-update-error`, uid: userDocUID,
		})
	}

}

async function sendEmailToAdminWhenUserIsFlagged(userData: FirebaseFirestore.DocumentData) {
	const mailData = {
		to: config.envvar.buzzarr_admin_email,
		from: config.envvar.buzzarr_alert_email,
		subject: `Buzzarr | User has been flagged after being flagged ${thresholdToFlagUserFlaggedByUsers} times by users!`,
		text: '.',
		html: `
                <p>Hi,</p>
                <p>The user with following detail has been flagged ${thresholdToFlagUserFlaggedByUsers} times by users:</p>
                <p>
                ${JSON.stringify(userData)}
                </p>
                <p>Thus, the user account status has been flagged!</p>
                <p>Thanks,</p>
                <p><b>Your Buzzarr Team</b></p>`,
	}
	await sendCustomEmail(mailData);
}

async function sendUserWelcomeEmailTo(userData: FirebaseFirestore.DocumentData) {
	const { email, firstName } = userData;
	const emailContent = `Welcome To Buzzarr! We will step you through the process 
    of finding providers for set of skills, 
    or serving those skills to an existing consumers in our platform.  
    Click the link below to get started using Buzzarr!`
	const mailData = {
		to: email,
		from: config.envvar.buzzarr_noreply_email,
		subject: 'Welcome To Buzzarr!',
		text: '.',
		html: welcomeToBuzzarrEmailTemplate({ firstName, emailContent }),
	}
	await sendCustomEmail(mailData);
}
