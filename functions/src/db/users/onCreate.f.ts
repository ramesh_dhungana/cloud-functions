import { functions } from "../../config/index";
import { getGenericCollectionDocumentRef } from "../../utils/collection_functions.util";
import { collectionNames } from "../../utils/externalized_variables.util";
import { logOnCollectionDocumentCreate, logOnFirestoreDocumentUpdateError } from "../../utils/logger.util";

export const onCreate = functions.firestore
    .document(`${collectionNames.users}/{userDocUID}`)
    .onCreate(async (snap, context) => {
        const userDocData = snap.data();
        const { userDocUID } = context.params;
        logOnCollectionDocumentCreate(
            { colName: collectionNames.users, colDocUID: userDocUID, colDocData: userDocData },
            {
                operation: `${collectionNames.users}-create`, uid: userDocUID,
            }
        );
        await updateQuoteWithAdditionalFields(userDocUID);
        return null;
    });


async function updateQuoteWithAdditionalFields(userDocUID: string) {
    try {
        const categoryDocRef = getGenericCollectionDocumentRef(
            { colName: collectionNames.users, colDocUID: userDocUID });
        await categoryDocRef.update({ uid: userDocUID });
    } catch (error) {
        logOnFirestoreDocumentUpdateError({
            functionName: 'updateQuoteWithAdditionalFields',
            message: "Error while updating newly created users to add fields like uid.",
            error,
        }, {
            operation: `${collectionNames.users}-update-error`, uid: userDocUID,
        });
    }
}
