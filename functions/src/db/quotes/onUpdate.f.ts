import { functions, config } from "../../config";
import { collectionDocumentFieldDataChanged } from "../../utils/collection_functions.util";
import { sendCustomEmail } from "../../utils/custom_email.util";
import { collectionNames, proposalStatus, quoteStatus } from "../../utils/externalized_variables.util";
import { getCollectionDocData, queryGenericCollection, updateCollectionDoc } from "../../utils/generic-collection-data.util";
import {
	logOnCollectionDocumentSoftDelete,
	logOnCollectionDocumentUpdate,
	logOnFirestoreDocumentUpdateError,
	logOnFirestoreCollectionQueryError,
	logOnFirestoreDocumentGetError,
} from "../../utils/logger.util";
import { sendEmailToInvitedEmails } from '../../utils/quote.util';
import { quoteStatusChangedEmailTemplate } from '../../email-templates/quote_status_changed';
const limitToEmailAdminOnQuoteFlaggedByUser = 3;
const thresholdToSoftDeleteOnQuoteFlaggedByUser = 5;

export const onUpdate = functions.firestore
	.document(`${collectionNames.quotes}/{quoteDocUID}`)
	.onUpdate(async (change, context) => {
		const { quoteDocUID } = context.params;
		const quoteDocDataBefore = change.before.data();
		const quoteDocDataAfter = change.after.data();
		logOnCollectionDocumentUpdate({
			colName: collectionNames.quotes, colDocUID: quoteDocUID,
			colDocDataBefore: quoteDocDataBefore, colDocDataAfter: quoteDocDataAfter,
		}, {
			operation: `${collectionNames.quotes}-update`, uid: quoteDocUID,
		});
		if (quoteToInvitedEmailsChanged(quoteDocDataBefore, quoteDocDataAfter)) {
			sendEmailToInvitedEmails(quoteDocDataAfter);
		}
		if (quoteIsSoftDeleted(quoteDocDataBefore, quoteDocDataAfter)) {
			logOnCollectionDocumentSoftDelete({
				colName: collectionNames.quotes, colDocUID: quoteDocUID,
				colDocDataBefore: quoteDocDataBefore, colDocDataAfter: quoteDocDataAfter,
			}, {
				operation: `${collectionNames.quotes}-soft-delete`, uid: quoteDocUID,
			});
		}
		if (quoteStatusChangedToClosed(quoteDocDataBefore, quoteDocDataAfter)) {
			const proposalParams = {
				colName: collectionNames.proposals,
				queryList: [
					{ key: 'quote', operator: '==', value: quoteDocUID },
					{
						key: 'status', operator: '==', value: quoteStatus.active,
					},
				],
			}
			try {
				const activeProposalSnaps = await queryGenericCollection(proposalParams);
				// we decline all these active proposals on closed quote
				activeProposalSnaps.forEach(async proposalSnap => {
					const proposalUID = proposalSnap.id;
					const toUpdateData = { status: proposalStatus.archived };
					const updateParams = {
						colName: collectionNames.proposals,
						colDocUID: proposalUID, colDocData: toUpdateData,
					}
					try {
						await updateCollectionDoc(updateParams);
					} catch (error) {
						logOnFirestoreDocumentUpdateError({
							functionName: 'updateCollectionDoc',
							message: "Error while updating closed quotes' proposal status to declined.",
							error,
						}, {
							operation: `${collectionNames.quotes}-update-error`, uid: quoteDocUID,
						})
					}
				})
			} catch (error) {
				logOnFirestoreCollectionQueryError({
					functionName: 'queryGenericCollection',
					message: "Error while fetching active proposals for closed quote.",
					error,
				}, {
					operation: `${collectionNames.quotes}-query-error`, queryParams: proposalParams,
				})
			}
		}
		if (quoteIsFlagged(quoteDocDataBefore, quoteDocDataAfter)) {
			const proposalParams = {
				colName: collectionNames.proposals,
				queryList: [
					{ key: 'quote', operator: '==', value: quoteDocUID },
					{
						key: 'status', operator: '==', value: proposalStatus.active,
					},
				],
			}
			try {
				const activeProposalSnaps = await queryGenericCollection(proposalParams);
				// we decline all these active proposals on closed quote
				activeProposalSnaps.forEach(async proposalSnap => {
					const proposalUID = proposalSnap.id;
					const toUpdateData = { status: proposalStatus.archived };
					const updateParams = {
						colName: collectionNames.proposals,
						colDocUID: proposalUID, colDocData: toUpdateData,
					}
					try {
						await updateCollectionDoc(updateParams);
					} catch (error) {
						logOnFirestoreDocumentUpdateError({
							functionName: 'updateCollectionDoc',
							message: "Error while updating closed quotes' proposal status to declined.",
							error,
						}, {
							operation: `${collectionNames.quotes}-update-error`, uid: quoteDocUID,
						})
					}

				})
			} catch (error) {
				logOnFirestoreCollectionQueryError({
					functionName: 'queryGenericCollection',
					message: "Error while fetching active proposals for closed quote.",
					error,
				}, {
					operation: `${collectionNames.quotes}-query-error`, queryParams: proposalParams,
				})
			}
			// now we send email to quote creater informing that their quote has been flagged. 
			try {
				const quoteUserParam = {
					colName: collectionNames.users,
					colDocUID: quoteDocDataAfter.user,
				}
				const quoteUserData = await getCollectionDocData(quoteUserParam);
				if (quoteUserData) {
					const { email, firstName } = quoteUserData;
					const emailContent = `We are sorry to inform you that your Quote on Buzzarr with title 
                                <strong>${quoteDocDataAfter.title}</strong> has been flagged as it
                                violates the Buzzarr policy. To know more about buzzarr policy, 
                                Please click on the link below:`
					const mailData = {
						to: email,
						from: config.envvar.buzzarr_buzzy_email,
						subject: 'Buzzarr | Your Quote is Flagged!',
						text: '.',
						html: quoteStatusChangedEmailTemplate({ firstName, emailContent }),
					}
					await sendCustomEmail(mailData);
				}
			} catch (error) {
				logOnFirestoreDocumentGetError({
					functionName: 'getCollectionDocData',
					message: "Error while getting user data of quote.",
					error,
				}, {
					operation: `${collectionNames.quotes}-query-error`, uid: quoteDocUID,
				})
			}
		}

		if (quoteUserFlagsChanged(quoteDocDataBefore, quoteDocDataAfter)) {
			const { userFlags } = quoteDocDataAfter;
			const userFlagsCountAfter: number = userFlags?.length;
			switch (userFlagsCountAfter) {
				case limitToEmailAdminOnQuoteFlaggedByUser:
					await sendEmailToAdminOnUserFlagsOnQuoteReachedLimit(quoteDocDataAfter)
					break;
				case thresholdToSoftDeleteOnQuoteFlaggedByUser:
					await softDeleteQuoteAfterUserFlaggedReachedThreshold(quoteDocDataAfter);
					break;
			}
			const userFlagsCountBefore = quoteDocDataBefore?.userFlags?.length;
			if (userFlagsCountBefore >= thresholdToSoftDeleteOnQuoteFlaggedByUser
				&& userFlagsCountAfter < thresholdToSoftDeleteOnQuoteFlaggedByUser) {
				const updateParams = {
					colName: collectionNames.quotes,
					colDocUID: quoteDocUID,
					colDocData: { deleted: false },
				}
				try {
					await updateCollectionDoc(updateParams);
				} catch (error) {
					logOnFirestoreDocumentUpdateError({
						functionName: 'updateCollectionDoc',
						message: "Error while updating soft deleted quote after count of userFlags is less then threshold.",
						error,
					}, {
						operation: `${collectionNames.quotes}-update-error`, uid: quoteDocUID,
					})
				}
			}
		}
		return null;
	});

function quoteStatusChangedToClosed(quoteDocDataBefore: FirebaseFirestore.DocumentData,
	quoteDocDataAfter: FirebaseFirestore.DocumentData) {
	const quoteStatusIsChanged = collectionDocumentFieldDataChanged(quoteDocDataBefore.status,
		quoteDocDataAfter.status);
	const quoteIsClosed = quoteDocDataAfter.status === quoteStatus.closed;
	return quoteStatusIsChanged && quoteIsClosed;
}

function quoteIsFlagged(quoteDocDataBefore: FirebaseFirestore.DocumentData,
	quoteDocDataAfter: FirebaseFirestore.DocumentData) {
	const flaggedValueChanged = collectionDocumentFieldDataChanged(quoteDocDataBefore.flagged,
		quoteDocDataAfter.flagged);
	const isFlagged = quoteDocDataAfter.flagged === true;
	return flaggedValueChanged && isFlagged;
}

function quoteIsSoftDeleted(quoteDocDataBefore: FirebaseFirestore.DocumentData,
	quoteDocDataAfter: FirebaseFirestore.DocumentData) {
	const deletedValueChanged = collectionDocumentFieldDataChanged(quoteDocDataBefore.deleted,
		quoteDocDataAfter.deleted);
	const isDeleted = quoteDocDataAfter.deleted === true;
	return deletedValueChanged && isDeleted;
}

function quoteToInvitedEmailsChanged(quoteDocDataBefore: FirebaseFirestore.DocumentData,
	quoteDocDataAfter: FirebaseFirestore.DocumentData) {
	const invitedEmailsChanged = collectionDocumentFieldDataChanged(quoteDocDataBefore.toInviteEmails,
		quoteDocDataAfter.toInviteEmails);
	const toInviteEmailsExists = quoteDocDataAfter.toInviteEmails && quoteDocDataAfter.toInviteEmails.length > 0;
	return invitedEmailsChanged && toInviteEmailsExists;
}

function quoteUserFlagsChanged(quoteDocDataBefore: FirebaseFirestore.DocumentData,
	quoteDocDataAfter: FirebaseFirestore.DocumentData) {
	return collectionDocumentFieldDataChanged(quoteDocDataBefore.userFlags, quoteDocDataAfter.userFlags);
}

async function sendEmailToAdminOnUserFlagsOnQuoteReachedLimit(quoteData: FirebaseFirestore.DocumentData) {
	const mailData = {
		to: config.envvar.buzzarr_admin_email,
		from: config.envvar.buzzarr_alert_email,
		subject: `Buzzarr | Quote has been flagged ${limitToEmailAdminOnQuoteFlaggedByUser} times by users!`,
		text: '.',
		html: `
                <p>Hi,</p>
                <p>The quote with following detail has been flagged ${limitToEmailAdminOnQuoteFlaggedByUser} times by users:</p>
                <p>
                ${JSON.stringify(quoteData)}
                </p>
                <p>Thanks,</p>
                <p><b>Your Buzzarr Team</b></p>`,
	}
	await sendCustomEmail(mailData);
}

async function softDeleteQuoteAfterUserFlaggedReachedThreshold(quoteData: FirebaseFirestore.DocumentData) {
	const quoteDocUID = quoteData.uid;
	const updateParams = {
		colName: collectionNames.quotes,
		colDocUID: quoteDocUID,
		colDocData: { deleted: true },
	}
	try {
		await updateCollectionDoc(updateParams);
		await sendEmailToAdminWhenQuoteIsSoftDeleted(quoteData);
	} catch (error) {
		logOnFirestoreDocumentUpdateError({
			functionName: 'updateCollectionDoc',
			message: "Error while soft deleting quote after flagged multiple times by users.",
			error,
		}, {
			operation: `${collectionNames.quotes}-update-error`, uid: quoteDocUID,
		})
	}

}

async function sendEmailToAdminWhenQuoteIsSoftDeleted(quoteData: FirebaseFirestore.DocumentData) {
	const mailData = {
		to: config.envvar.buzzarr_admin_email,
		from: config.envvar.buzzarr_alert_email,
		subject: `Buzzarr | Quote has been soft deleted after being flagged ${thresholdToSoftDeleteOnQuoteFlaggedByUser} times by users!`,
		text: '.',
		html: `
                <p>Hi,</p>
                <p>The quote with following detail has been flagged ${thresholdToSoftDeleteOnQuoteFlaggedByUser} times by users:</p>
                <p>
                ${JSON.stringify(quoteData)}
                </p>
                <p>Thus, the quote has been soft deleted!</p>
                <p>Thanks,</p>
                <p><b>Your Buzzarr Team</b></p>`,
	}
	await sendCustomEmail(mailData);
}