import { functions } from "../../config/index";
import { getGenericCollectionDocumentRef } from "../../utils/collection_functions.util";
import { collectionNames } from "../../utils/externalized_variables.util";
import { logOnCollectionDocumentCreate, logOnFirestoreDocumentUpdateError } from "../../utils/logger.util";
import { sendEmailToInvitedEmails } from '../../utils/quote.util';

export const onCreate = functions.firestore
    .document(`${collectionNames.quotes}/{quoteDocUID}`)
    .onCreate(async (snap, context) => {
        const quoteDocData = snap.data();
        const { quoteDocUID } = context.params;
        logOnCollectionDocumentCreate(
            { colName: collectionNames.quotes, colDocUID: quoteDocUID, colDocData: quoteDocData },
            {
                operation: `${collectionNames.quotes}-create`, uid: quoteDocUID,
            }
        );
        await updateQuoteWithAdditionalFields(quoteDocUID)
        sendEmailToInvitedEmails(quoteDocData);
        return null;
    });


async function updateQuoteWithAdditionalFields(quoteDocUID: string) {
    try {
        const categoryDocRef = getGenericCollectionDocumentRef(
            { colName: collectionNames.quotes, colDocUID: quoteDocUID });
        await categoryDocRef.update({ uid: quoteDocUID, submittedProposalsCount: 0 });
    } catch (error) {
        logOnFirestoreDocumentUpdateError({
            functionName: 'updateQuoteWithAdditionalFields',
            message: "Error while updating newly created quotes to add fields like uid.",
            error,
        }, {
            operation: `${collectionNames.quotes}-update-error`, uid: quoteDocUID,
        });
    }
}


