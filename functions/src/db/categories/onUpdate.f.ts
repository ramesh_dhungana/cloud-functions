import { functions } from "../../config/index";
import { collectionDocumentFieldDataChanged } from "../../utils/collection_functions.util";
import {
	collectionNames, quoteStatus,
} from "../../utils/externalized_variables.util";
import {
	queryGenericCollection,
	updateCollectionDoc,
} from "../../utils/generic-collection-data.util";
import {
	logOnCollectionDocumentUpdate,
	logOnFirestoreCollectionQueryError,
	logOnFirestoreDocumentUpdateError,
	logOnFirestoreTriggerError,
} from "../../utils/logger.util";

export const onUpdate = functions.firestore
	.document(`${collectionNames.categories}/{categoryDocUID}`)
	.onUpdate(async (change, context) => {
		const categoryDocDataBefore = change.before.data();
		const categoryDocDataAfter = change.after.data();
		const { categoryDocUID } = context.params;
		logOnCollectionDocumentUpdate({
			colName: collectionNames.categories, colDocUID: categoryDocUID,
			colDocDataBefore: categoryDocDataBefore, colDocDataAfter: categoryDocDataAfter,
		}, {
			operation: `${collectionNames.categories}-update`, uid: categoryDocUID,
		});
		if (categoryNameHasChanged(categoryDocDataBefore, categoryDocDataAfter)) {
			updateAllQuotesAssociatedWithCategory(categoryDocDataAfter);
		};
	})


function categoryNameHasChanged(categoryDocDataBefore: FirebaseFirestore.DocumentData,
	categoryDocDataAfter: FirebaseFirestore.DocumentData) {
	return collectionDocumentFieldDataChanged(categoryDocDataBefore.name, categoryDocDataAfter.name);
}

function updateAllQuotesAssociatedWithCategory(categoryDocDataAfter: FirebaseFirestore.DocumentData) {
	try {
		const categoryDocUID = categoryDocDataAfter.uid;
		const qutoesParams = {
			colName: collectionNames.quotes,
			queryList: [
				{ key: 'category.uid', operator: '==', value: categoryDocUID },
				{ key: 'status', operator: '==', value: quoteStatus.active },
			],
		}
		queryGenericCollection(qutoesParams)
			.then(activeQuoteSnaps => {
				activeQuoteSnaps.forEach(async quoteSnap => {
					const quoteUID = quoteSnap.id;
					const toUpdateData = { category: categoryDocDataAfter };
					const updateParams = {
						colName: collectionNames.quotes,
						colDocUID: quoteUID, colDocData: toUpdateData,
					}
					updateCollectionDoc(updateParams)
						.catch(error => {
							logOnFirestoreDocumentUpdateError({
								functionName: 'updateCollectionDoc',
								message: "Error while updating active quotes' category field to new updated value.",
								error,
							}, {
								operation: `${collectionNames.quotes}-update-error`, uid: quoteUID,
							})
						})
				})
			})
			.catch(error => {
				logOnFirestoreCollectionQueryError({
					functionName: 'queryGenericCollection',
					message: "Error while fetching active quotes.",
					error: error,
				}, {
					operation: `${collectionNames.quotes}-query-error`,
					queryParams: qutoesParams,
				})
			})
	} catch (error) {
		logOnFirestoreTriggerError({
			functionName: 'updateAllQuotesAssociatedWithCategory::queryGenericCollection',
			message: "Error while querying activte quotes for given category",
			error,
		}, {
			operation: `${collectionNames.quotes}-error`,
		})
	}
}
