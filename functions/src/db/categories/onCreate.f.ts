import { functions } from "../../config/index";
import { getGenericCollectionDocumentRef } from "../../utils/collection_functions.util";
import { collectionNames } from "../../utils/externalized_variables.util";
import {
    logOnCollectionDocumentCreate,
    logOnFirestoreDocumentUpdateError,
} from "../../utils/logger.util";

export const onCreate = functions.firestore
    .document(`${collectionNames.categories}/{categoryDocUID}`)
    .onCreate(async (snap, context) => {
        const categoryDocData = snap.data();
        const { categoryDocUID } = context.params;
        logOnCollectionDocumentCreate(
            { colName: collectionNames.categories, colDocUID: categoryDocUID, colDocData: categoryDocData },
            {
                operation: `${collectionNames.categories}-create`, uid: categoryDocUID,
            }
        );
        await addUIDToCreatedCategoryDocument(categoryDocUID);
        return null;
    });


async function addUIDToCreatedCategoryDocument(categoryDocUID: string) {
    try {
        const categoryDocRef = getGenericCollectionDocumentRef(
            { colName: collectionNames.categories, colDocUID: categoryDocUID });
        await categoryDocRef.update({ uid: categoryDocUID });
    } catch (error) {
        logOnFirestoreDocumentUpdateError({
            functionName: 'addUIDToCreatedCategoryDocument',
            message: "Error while updating closed quotes' proposal status to archived.",
            error,
        }, {
            operation: `${collectionNames.categories}-update-error`, uid: categoryDocUID,
        });
    }
}
