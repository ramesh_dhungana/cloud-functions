import { functions, config } from "../../config";
import { sendCustomEmail } from "../../utils/custom_email.util";
import {
	collectionNames,
	proposalStatus,
} from "../../utils/externalized_variables.util";
import { getCollectionDocData, queryGenericCollection, updateCollectionDoc } from "../../utils/generic-collection-data.util";
import {
	logOnCollectionDocumentSoftDelete,
	logOnCollectionDocumentUpdate,
	logOnFirestoreDocumentUpdateError,
	logOnFirestoreCollectionQueryError,
	logOnFirestoreDocumentGetError,
} from "../../utils/logger.util";
import { collectionDocumentFieldDataChanged } from "../../utils/collection_functions.util";
import { proposalStatusChangedEmailTemplate } from '../../email-templates/proposal_status_changed';

export const onUpdate = functions.firestore
	.document(`${collectionNames.proposals}/{proposalDocUID}`)
	.onUpdate(async (change, context) => {
		const { proposalDocUID } = context.params;
		const proposalDocDataBefore = change.before.data();
		const proposalDocDataAfter = change.after.data();
		logOnCollectionDocumentUpdate({
			colName: collectionNames.proposals, colDocUID: proposalDocUID,
			colDocDataBefore: proposalDocDataBefore, colDocDataAfter: proposalDocDataAfter,
		}, { operation: `${collectionNames.proposals}-update`, uid: proposalDocUID });

		if (proposalIsSoftDeleted(proposalDocDataBefore, proposalDocDataAfter)) {
			logOnCollectionDocumentSoftDelete({
				colName: collectionNames.proposals, colDocUID: proposalDocUID,
				colDocDataBefore: proposalDocDataBefore, colDocDataAfter: proposalDocDataAfter,
			}, { operation: `${collectionNames.proposals}-soft-delete`, uid: proposalDocUID });
		}
		if (proposalStatusChangedToAccepted(proposalDocDataBefore, proposalDocDataAfter)) {
			const proposalParams = {
				colName: collectionNames.proposals,
				queryList: [
					{ key: 'quote', operator: '==', value: proposalDocDataAfter.quote },
					{
						key: 'status', operator: '==', value: proposalStatus.active,
					},
				],
			}
			try {
				const activeProposalSnaps = await queryGenericCollection(proposalParams);
				// we decline all these active proposals if the any one of the proposal on quote is accepted 
				activeProposalSnaps.forEach(async proposalSnap => {
					const proposalUID = proposalSnap.id;
					const toUpdateData = { status: proposalStatus.archived };
					const updateParams = {
						colName: collectionNames.proposals,
						colDocUID: proposalUID,
						colDocData: toUpdateData,
					}
					try {
						await updateCollectionDoc(updateParams);
					} catch (error) {
						logOnFirestoreDocumentUpdateError({
							functionName: 'proposalStatusChangedToAccepted:updateCollectionDoc',
							message: "Error while updating proposal status to declined.",
							error,
						}, { operation: `${collectionNames.proposals}-update-error`, uid: proposalDocUID })
					}
				})
			} catch (error) {
				logOnFirestoreCollectionQueryError({
					functionName: 'proposalStatusChangedToAccepted:queryGenericCollection',
					message: "Error while fetching active proposals.",
					error,
				}, { operation: `${collectionNames.proposals}-query-error`, queryParams: proposalParams })
			}
		}
		if (proposalIsFlagged(proposalDocDataBefore, proposalDocDataAfter)) {
			// now we send email to quote creater informing that their quote has been flagged. 
			try {
				const proposalUserParam = {
					colName: collectionNames.users,
					colDocUID: proposalDocDataAfter.user,
				}
				const proposalUserData = await getCollectionDocData(proposalUserParam);
				const proposalQuoteData = await getCollectionDocData({
					colName: collectionNames.quotes,
					colDocUID: proposalDocDataAfter.quote,
				});
				if (proposalUserData && proposalQuoteData) {
					const { firstName } = proposalUserData;
					const emailContent = `We are sorry to inform you that your Proposal on Buzzarr with title
                                <strong>${proposalDocDataAfter.title}</strong> on quote <strong>${proposalQuoteData?.title}</strong> has been flagged as it
                                violates the Buzzarr policy. To know more about buzzarr policy, 
                                Please click on the link below:`
					const mailData = {
						to: proposalUserData.email,
						from: config.envvar.buzzarr_buzzy_email,
						subject: 'Buzzarr | Your Proposal is Flagged!',
						text: '.',
						html: proposalStatusChangedEmailTemplate({ firstName, emailContent }),
					}
					await sendCustomEmail(mailData);
				}
			} catch (error) {
				logOnFirestoreDocumentGetError({
					functionName: 'getCollectionDocData',
					message: "Error while getting user data of proposal.",
					error,
				}, { operation: `${collectionNames.proposals}-query-error`, uid: proposalDocUID })
			}

		}
		return null;
	});

function proposalStatusChangedToAccepted(proposalDocDataBefore: FirebaseFirestore.DocumentData,
	proposalDocDataAfter: FirebaseFirestore.DocumentData) {
	const proposalStatusIsChanged = collectionDocumentFieldDataChanged(proposalDocDataBefore.status,
		proposalDocDataAfter.status);
	const proposalIsClosed = proposalDocDataAfter.status === proposalStatus.accepted;
	return proposalStatusIsChanged && proposalIsClosed;
}

function proposalIsFlagged(proposalDocDataBefore: FirebaseFirestore.DocumentData,
	proposalDocDataAfter: FirebaseFirestore.DocumentData) {
	const flaggedValueIsChanged = collectionDocumentFieldDataChanged(proposalDocDataBefore.flagged,
		proposalDocDataAfter.flagged);
	const isFlagged = proposalDocDataAfter.flagged === true;
	return flaggedValueIsChanged && isFlagged;
}

function proposalIsSoftDeleted(proposalDocDataBefore: FirebaseFirestore.DocumentData,
	proposalDocDataAfter: FirebaseFirestore.DocumentData) {
	const deletedValueIsChanged = collectionDocumentFieldDataChanged(proposalDocDataBefore.deleted,
		proposalDocDataAfter.deleted);
	const isDeleted = proposalDocDataAfter.deleted === true;
	return deletedValueIsChanged && isDeleted;
}
