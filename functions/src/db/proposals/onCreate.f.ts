import { functions, firebaseAdmin } from "../../config/index";
import { collectionNames } from "../../utils/externalized_variables.util";
import { getGenericCollectionDocumentRef } from "../../utils/collection_functions.util";
import {
	logOnCollectionDocumentCreate,
	logOnFirestoreDocumentUpdateError,
} from "../../utils/logger.util";

export const onCreate = functions.firestore
	.document(`${collectionNames.proposals}/{proposalDocUID}`)
	.onCreate((snap, context) => {
		const proposalDocData = snap.data();
		const { proposalDocUID } = context.params;
		logOnCollectionDocumentCreate(
			{ colName: collectionNames.proposals, colDocUID: proposalDocUID, colDocData: proposalDocData },
			{
				operation: `${collectionNames.proposals}-create`, uid: proposalDocUID,
			}
		);
		updateNumberOfSubmittedProposalsCountToAssociatedQuote(proposalDocData.quote);
		return null;
	});


function updateNumberOfSubmittedProposalsCountToAssociatedQuote(
	quoteUID: string) {
	const quoteDocRef = getGenericCollectionDocumentRef({ colName: collectionNames.quotes, colDocUID: quoteUID });
	const toUpdateQuoteData = {
		submittedProposalsCount: firebaseAdmin.firestore.FieldValue.increment(1),
	}
	quoteDocRef.update(toUpdateQuoteData)
		.then(() => {
			console.log(`Success: submittedProposalsCount increased by 1 
                    for quote with uid::${quoteUID}, 
                    newCount:${toUpdateQuoteData.submittedProposalsCount}`);
		}).catch(error => {
			logOnFirestoreDocumentUpdateError({
				functionName: 'updateNumberOfSubmittedProposalsCountToAssociatedQuote',
				message: `Error while increasing the submittedProposalsCount to 
                        the quote ${quoteUID}. We are retrying!`,
				error: error,
			}, {
				operation: `${collectionNames.quotes}-update-error`, uid: quoteUID,
			})
			// retry once more.
			quoteDocRef.update(toUpdateQuoteData).catch(err => {
				logOnFirestoreDocumentUpdateError({
					functionName: 'updateNumberOfSubmittedProposalsCountToAssociatedQuote',
					message: `Error while increasing the submittedProposalsCount to 
                        the quote ${quoteUID}. We are retrying!`,
					error: err,
				}, {
					operation: `${collectionNames.quotes}-update-error`, uid: quoteUID,
				})
			});
		})
}
