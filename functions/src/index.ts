import admins = require('./db/admins/index');
import users = require('./db/users/index');
import quotes = require('./db/quotes/index');
import proposals = require('./db/proposals/index');
import categories = require('./db/categories/index');
import locations = require('./db/locations/index');
import https = require('./https/index');
import schedulers = require('./schedulers/index');

exports.https = https;
exports.users = users;
exports.quotes = quotes;
exports.proposals = proposals;
exports.locations = locations;
exports.categories = categories;
exports.schedulers = schedulers;
exports.admins = admins;













