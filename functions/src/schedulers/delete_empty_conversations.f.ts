import { functions } from '../config/index';
import { collectionNames } from '../utils/externalized_variables.util';
import { queryGenericCollection } from '../utils/generic-collection-data.util';
import { logOnFirestoreDocumentDeleteError, logOnScheduleFunctionError } from '../utils/logger.util';
import { thirtyMinutesInMilliseconds } from '../utils/date.util';
import { getGenericCollectionDocumentRef } from '../utils/collection_functions.util';

export const deleteEmptyConversations = functions.pubsub.schedule('0 2 * * *')
	.timeZone('America/New_York') // Users can choose timezone - default is America/Los_Angeles
	.onRun(async (context) => {
		console.log('This will be run every day at 2.00 AM!');
		await performDeleteEmptyConversations()
		return null;
	});


async function performDeleteEmptyConversations() {
	try {
		const conversationParams = {
			colName: collectionNames.conversations,
			queryList: [
				{ key: 'messages', operator: '==', value: [] },
			],
		}
		const converstationSnaps = await queryGenericCollection(conversationParams);
		converstationSnaps.forEach(async conSnap => {
			const conversationData = conSnap.data();
			const conversationDocUID = conSnap.id;
			if (conversationIsEmpty(conversationData)) {
				console.log(`Empty Converstation with uid ${conversationDocUID}.`);
				const createdDate: Date = conversationData.createdAt.toDate();
				const thirtyMinutesBack = Date.now() - thirtyMinutesInMilliseconds;
				if (createdDate.getTime() < thirtyMinutesBack) {
					console.log(`Empty converstation with uid ${conversationDocUID} is already 
                    ${thirtyMinutesInMilliseconds / (60000)} minutes old. Thus, deleting it.`);
					const conversationDocRef = getGenericCollectionDocumentRef({
						colName: collectionNames.conversations,
						colDocUID: conversationDocUID,
					})
					conversationDocRef.delete()
						.then(response => {
							console.log(`Delete Success::Empty converstation with uid ${conversationDocUID} deleted`)
						})
						.catch(error => {
							logOnFirestoreDocumentDeleteError({
								functionName: 'performDeleteEmptyConversations',
								message: `Error while deleting the conversation ${conversationDocUID}`,
								error,
							}, {
								operation: `${collectionNames.users}-delete-error`, uid: conversationDocUID,
							})
						});
				}
			}
		});
	} catch (error) {
		logOnScheduleFunctionError({
			functionName: 'performDeleteEmptyConversations',
			message: "Error while fetching empty conversations user.",
			error,
		})
	}
}

function conversationIsEmpty(conversationData: FirebaseFirestore.DocumentData) {
	return conversationData?.messages && conversationData.messages.length === 0;
}
