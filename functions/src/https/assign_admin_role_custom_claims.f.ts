import { functions, cors } from "../config";
import {
    hasAuthorizedRoles,
    isAuthenticated, isPOSTRequest,
    methodNotAllowedResponse,
} from "../utils/middleware.util";
import { Request, Response } from "express";
import { setUserRolesCustomClaims } from "../utils/custom_claims.util";
import { adminRoleNames } from "../utils/externalized_variables.util";

export const assignAdminRoleCustomClaims = functions.https.onRequest(
    (request: Request, response: Response) => {
        cors(request, response, async () => {
            if (!isPOSTRequest(request)) { return methodNotAllowedResponse(response); };
            return isAuthenticated(request, response, async () => {
                return hasAuthorizedRoles({ hasRoles: ['buzzarrAdmin'] }, request, response,
                    async () => {
                        return await performAssignAdminRoleCustomClaims(request, response);
                    });
            });
        });
    });


async function performAssignAdminRoleCustomClaims(request: Request, response: Response) {
    const { roles, uid } = request.body;
    try {
        const filteredAdminRoles = roles.filter((role: string) => adminRoleNames.find(adminR => role));
        console.log('Request body roles::', filteredAdminRoles);
        for await (const roleName of roles) {
            await setUserRolesCustomClaims(uid, roleName)
                .catch(error => {
                    console.error(`Error on setUserRolesCustomClaims(${uid},${roleName}):: ${error}`);
                    return response.status(401).send({ success: false, message: 'Error in setting user role', error: error });
                });
        }
        return response.status(200).send({ success: true, message: 'Role is added successfully!' });
    } catch (error) {
        console.error(`Error on performAssignUserRoleCustomClaims:: ${error}`);
        return response.status(401).send({ success: false, message: 'Error in setting user role', error: error });
    }
}