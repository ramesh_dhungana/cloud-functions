import { functions, cors } from "../config";
import {
    isAuthenticated, isPOSTRequest,
    methodNotAllowedResponse,
} from "../utils/middleware.util";
import { Request, Response } from "express";
import { setUserRolesCustomClaims } from "../utils/custom_claims.util";

export const assignUserRoleCustomClaims = functions.https.onRequest((request: Request, response: Response) => {
    cors(request, response, () => {
        if (!isPOSTRequest(request)) { return methodNotAllowedResponse(response); };
        return isAuthenticated(request, response, async () => {
            return await performAssignUserRoleCustomClaims(request, response);
        });
    });
});


async function performAssignUserRoleCustomClaims(request: Request, response: Response) {
    const { userUID } = response.locals.authUserData;
    const { roles } = request.body;
    try {
        console.log('Request body roles::', roles);
        for await (const roleName of roles) {
            await setUserRolesCustomClaims(userUID, roleName)
                .catch(error => {
                    console.error(`Error on setUserRolesCustomClaims(${userUID},${roleName}):: ${error}`);
                    return response.status(401).send({ success: false, message: 'Error in setting user role', error: error });
                });
        }
        return response.status(200).send({ success: true, message: 'User role added successfully!' });
    } catch (error) {
        console.error(`Error on performAssignUserRoleCustomClaims:: ${error}`);
        return response.status(401).send({ success: false, message: 'Error in setting user role', error: error });
    }
}