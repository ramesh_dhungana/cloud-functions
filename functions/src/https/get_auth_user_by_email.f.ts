import { functions, cors, firebaseAdmin } from "../config";
import { isAuthenticated, isPOSTRequest, methodNotAllowedResponse } from "../utils/middleware.util";
import { Request, Response } from "express";

export const getAuthUserByEmail = functions.https.onRequest(
    (request: Request, response: Response) => {
        cors(request, response, () => {
            if (!isPOSTRequest(request)) { return methodNotAllowedResponse(response); };
            return isAuthenticated(request, response, () => {
                return performGetAuthUserByEmail(request, response);
            });
        });
    });


async function performGetAuthUserByEmail(request: Request, response: Response) {
    const { email } = request.body;
    firebaseAdmin.auth().getUserByEmail(email)
        .then((userRecord) => {
            console.log(`performGetAuthUserByEmail(${email}) Successfully fetched user data: ${userRecord.toJSON()}`);
            return response.status(200).send({
                success: true,
                message: 'Successfully fetched auth user data.',
                data: userRecord,
            });
        })
        .catch((error) => {
            console.log(`performGetAuthUserByEmail(${email}) Error fetching user data: ${error}`);
            return response.status(500).send({
                success: false,
                message: 'Error in fetching auth user data.',
                error: error,
            });
        });
}